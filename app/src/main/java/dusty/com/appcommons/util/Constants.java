package dusty.com.appcommons.util;

import com.dusty.commons.web.dto.TokenServiceParams;

/**
 * @author .: Chukwudum Ekwueme
 * email ..: chidumekwueme@gmail.com, chukwudum.ekwueme@cwlgroup.com
 * created : Oct 01, 2018 12:43 PM
 */
public interface Constants {

    String adminClientSecret = "xdX(z3J\\#k(mcJE84Y,e*}b:{t4TU#\\k";
    String adminClientID = "BILLS_N_PAY_APP_CLIENT";
    String appBaseURL = "https://159.122.189.172";

    TokenServiceParams tokenServiceParams = TokenServiceParams.builder()
            .adminClientID(adminClientID)
            .adminClientSecret(adminClientSecret)
            .adminScope("BIKON_APP")
            .adminGrantType("client_credentials")
            .adminUserName("")
            .tokenBaseURL(appBaseURL)
            .adminPassword("")
            .userTokenClientId("BIKON_USER_CLIENT")
            .userTokenClientSecret("WDS=CX2-FA$6B$N-G+JY6NW#65&AYBKADE?6ZT&H9!%5!E*%2K_*4P@_JP?2KFSV")
            .userTokenGrantType("password").build();
}
