package dusty.com.appcommons;

import android.databinding.DataBindingUtil;
import android.os.Bundle;

import com.dusty.commons.activity.BaseAppCompatActivity;
import com.dusty.commons.util.AppUtil;
import com.jakewharton.rxbinding2.view.RxView;


import dusty.com.appcommons.databinding.ActivityMainBinding;
import dusty.com.appcommons.vm.MainVM;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public class MainActivity extends BaseAppCompatActivity {

    CompositeDisposable compositeDisposable = new CompositeDisposable();
    private ActivityMainBinding binding;
    private MainVM viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        binding.setViewModel(viewModel);
        setEnableLog(true);
        viewModel = new MainVM(appUtil);
       setupBindings();
    }

    private void setupBindings(){



        Disposable onClickDeviceRegistration = RxView.clicks(binding.getUserDetailsButton)
                .doOnNext(aVoid -> {
            viewModel.getDeviceRegistration();
        }).subscribe();

       Disposable onClickNextActivity = RxView.clicks(binding.nextActivityButton).doOnNext(aVoid -> {
            getToNextActivity();
       }).subscribe();

        compositeDisposable.add(onClickDeviceRegistration);
        compositeDisposable.add(onClickNextActivity);

    }

    private void getToNextActivity(){
        startActivity(new SecondActivityIntentBuilder().build(this));
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        compositeDisposable.clear();
    }
}
