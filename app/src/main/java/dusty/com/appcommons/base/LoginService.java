package dusty.com.appcommons.base;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.dusty.commons.util.AppUtil;
import com.dusty.commons.web.dto.GenerateLoginTokenResponse;
import com.dusty.commons.web.dto.UserLoginRequest;


/**
 * @author .: Chukwudum Ekwueme
 * email ..: chidumekwueme@gmail.com, chukwudum.ekwueme@cwlgroup.com
 * created : 12/4/16
 */
public class LoginService extends TestBaseService {

    public static String OPERATION_LOG_IN = "operation.user.log.in";
    public static String USER_LOGIN_REQUEST = "user.token.request";


    public LoginService(Context context, Intent intent) {
        super(context, intent);
    }


    public void loginUser(){

        AppUtil.debug("about getting log in request from intent");
        UserLoginRequest userLoginRequest  = (UserLoginRequest) intent.getSerializableExtra(USER_LOGIN_REQUEST);


        AppUtil.debug("about to login user with the user login : " + userLoginRequest.getUsername());

        GenerateLoginTokenResponse generateLoginTokenResponse =
                generateLoginToken(userLoginRequest.getUsername(),
                        userLoginRequest.getPassword());


        Bundle bundle = new Bundle();
        bundle.putString(com.dusty.commons.web.service.util.ServiceConstants.OPERATION, OPERATION_LOG_IN);

        if(!generateLoginTokenResponse.isWasSuccessFul()){
            bundle.putSerializable(com.dusty.commons.web.service.util.ServiceConstants.API_ERROR, generateLoginTokenResponse.getError());
            getRecieverHandle().send(com.dusty.commons.web.service.util.ServiceConstants.FAILED, bundle);

        }else{
            AppUtil.debug("successful log in");
            getRecieverHandle().send(com.dusty.commons.web.service.util.ServiceConstants.SUCCESS, bundle);
            //save successful login credentials in case of token future calls
            oauthUtils.saveSuccessfulLoginCredentials(userLoginRequest.getUsername(), userLoginRequest.getPassword());

        }

    }
}
