package dusty.com.appcommons.base;

import android.content.Context;
import android.content.Intent;

import com.dusty.commons.RxClientRxService;
import com.dusty.commons.web.dto.AuthToken;
import com.dusty.commons.web.dto.TokenServiceParams;
import com.dusty.commons.web.service.TokenService;

import dusty.com.appcommons.client.TestClient;
import retrofit2.Call;

/**
 * @author .: Chukwudum Ekwueme
 * email ..: chidumekwueme@gmail.com, chukwudum.ekwueme@cwlgroup.com
 * created : 5/21/17 12:38 AM
 */
public class TestBaseService extends TokenService<TestClient> {


    public TestBaseService(Context context, Intent intent) {
        super(context,intent,
                TokenServiceParams.builder()
                        .adminClientID("BIKON_APP_CLIENT")
                        .adminClientSecret("Y$&X#S7F=3G45TE5Q66@KDK&K=7V7$QFDG2L#^$C492=&MM^P6JXP7XHXK&6B&6S")
                        .adminScope("BIKON_APP")
                        .adminGrantType("client_credentials")
                        .adminUserName("")
                        .adminPassword("")
                        .userTokenClientId("BIKON_USER_CLIENT")
                        .userTokenClientSecret("WDS=CX2-FA$6B$N-G+JY6NW#65&AYBKADE?6ZT&H9!%5!E*%2K_*4P@_JP?2KFSV")
                        .userTokenGrantType("password")

                .build(), TestClient.class, true);
    }



    @Override
    public Call<AuthToken> obtainTransactionToken(TestClient service,
                                                  String username,
                                                  String password,
                                                  String grantType,
                                                  String scope,
                                                  String clientId,
                                                  String clientSecret) {

        return null;
    }


}
