package dusty.com.appcommons.vm;

import android.databinding.Observable;
import android.databinding.ObservableField;

import com.dusty.commons.JsonPlaceholderClientRxService;
import com.dusty.commons.RxClientRxService;
import com.dusty.commons.util.AppUtil;
import com.dusty.commons.web.dto.AuthToken;

import dusty.com.appcommons.client.TestService;
import dusty.com.appcommons.util.Constants;
import io.reactivex.disposables.Disposable;

/**
 * @author .: Chukwudum Ekwueme
 * email ..: chidumekwueme@gmail.com, chukwudum.ekwueme@cwlgroup.com
 * created : Sep 30, 2018 11:11 AM
 */
public class MainVM {

    private AppUtil appUtil;

    public ObservableField<String> deviceId = new ObservableField<>();
    private TestService testService;
    private RxClientRxService rxService;

    private JsonPlaceholderClientRxService jsonPlaceholderClientRxService;

    public MainVM(AppUtil appUtil){
        //instantiate instance of the client service
        jsonPlaceholderClientRxService = JsonPlaceholderClientRxService.builder()
                .build();

        this.appUtil = appUtil;
        testService = TestService.builder()
                .withTokenServiceParams(Constants.tokenServiceParams)
                .withUtils(appUtil)
                .build();

        rxService = RxClientRxService.builder()
                .withTokenServiceParams(Constants.tokenServiceParams)
                .withUtils(appUtil)
                .build();


        deviceId.addOnPropertyChangedCallback(new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable sender, int propertyId) {
                AppUtil.log("property changed to {} ", deviceId.get());
            }
        });
    }


    public void getDeviceRegistration(){

        Disposable disposable1 = jsonPlaceholderClientRxService.getPhotos().subscribe(s -> {
            if(s.getResponse() != null){
                AppUtil.error("photos {}", s.getResponse().get(0).getThumbnailUrl());
            }
        });


        Disposable disposable =  rxService.getPosts()
              /*  .registerDevice(RegisterDeviceRequest.builder().deviceType("ANDROID")
                        .build())*/
                .subscribe(s -> {
                    if(s.getResponse() != null)
                        deviceId.set(s.getResponse().get(0).getTitle());
                    AppUtil.debug("{}" , s.getRxResponseStatus());
                });
    }


    private void handleResults(AuthToken authToken){
        AppUtil.debug("this is the token : " + authToken.toString());
    }

    private void handleError(Throwable t){
        AppUtil.error(t.getMessage());
    }





}
