package dusty.com.appcommons.client.dto;

import com.dusty.commons.annotation.ApiResponse;

import java.io.Serializable;

import lombok.Data;

/**
 * @author .: Chukwudum Ekwueme
 * email ..: chidumekwueme@gmail.com, chukwudum.ekwueme@cwlgroup.com
 * created : Oct 01, 2018 9:54 AM
 */
@ApiResponse
@Data
public class RegisterDeviceResponse implements Serializable {

    private String deviceId;

}
