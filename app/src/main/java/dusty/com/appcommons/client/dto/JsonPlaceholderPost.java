package dusty.com.appcommons.client.dto;

import java.io.Serializable;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class JsonPlaceholderPost implements Serializable {
    private int userId;
    private int id;
    private String title;
    private String body;


}
