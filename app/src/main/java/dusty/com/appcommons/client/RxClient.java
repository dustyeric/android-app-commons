package dusty.com.appcommons.client;

import com.dusty.commons.RegisterDeviceResponseApiResponse;
import com.dusty.commons.SampleResponseApiResponse;
import com.dusty.commons.annotation.CommonsService;
import com.dusty.commons.annotation.GenerateRxService;
import com.dusty.commons.annotation.GeneratedResponse;
import com.dusty.commons.annotation.TokenEndPoint;
import com.dusty.commons.annotation.constants.Security;
import com.dusty.commons.web.dto.AuthToken;


import dusty.com.appcommons.client.dto.Posts;
import dusty.com.appcommons.client.dto.RegisterDeviceRequest;
import dusty.com.appcommons.client.dto.RegisterDeviceResponse;
import dusty.com.appcommons.client.dto.SampleRequest;
import dusty.com.appcommons.client.dto.SampleResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * @author .: Chukwudum Ekwueme
 * email ..: chidumekwueme@gmail.com, chukwudum.ekwueme@cwlgroup.com
 * created : Sep 30, 2018 12:37 PM
 */
@GenerateRxService(baseURL = "https://159.122.189.172")
public interface RxClient {


    @TokenEndPoint
    @FormUrlEncoded
    @POST("/oauth-service/uaa/api/oauth2/token")
    Call<AuthToken> obtainTransactionToken(@Field("grant_type") String grantType,
                                           @Field("username") String username,
                                           @Field("password")String password);


    @GeneratedResponse(RegisterDeviceResponse.class)
    @CommonsService
    @POST("/api-service/api/billsnpay/app/register-device")
    Call<RegisterDeviceResponseApiResponse> registerDevice(@Body RegisterDeviceRequest registerDeviceRequest);

    @CommonsService(debug = true,
            responseClass = Posts.class,
            baseURL = "https://jsonplaceholder.typicode.com", security = Security.BASIC)
    @GET("/posts")
    Call<Posts> getPostsWithBasicAuth();

    @GeneratedResponse(SampleResponse.class)
    @CommonsService
    @POST("/test/path")
    Call<SampleResponseApiResponse> anotherService(@Body SampleRequest sampleRequest, int param2);


    @CommonsService(debug = true,
            responseClass = Posts.class, baseURL = "https://jsonplaceholder.typicode.com", security = Security.NONE)
    @GET("/posts")
    Call<Posts> getPosts();
}
