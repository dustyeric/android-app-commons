package dusty.com.appcommons.client;


import com.dusty.commons.SampleResponseApiResponse;
import com.dusty.commons.annotation.CommonsService;
import com.dusty.commons.annotation.GenerateAndroidIntentService;
import com.dusty.commons.annotation.GenerateBaseService;
import com.dusty.commons.annotation.GeneratedResponse;
import com.dusty.commons.annotation.TokenEndPoint;
import com.dusty.commons.web.dto.AuthToken;

import java.util.List;
import dusty.com.appcommons.client.dto.Posts;
import dusty.com.appcommons.client.dto.SampleRequest;
import dusty.com.appcommons.client.dto.SampleResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.POST;

/**
 * @author .: Chukwudum Ekwueme
 * email ..: chidumekwueme@gmail.com, chukwudum.ekwueme@cwlgroup.com
 * created : Mar 17, 2018 12:32 PM
 */
@GenerateBaseService(adminClientId = "test", adminClientSecret = "secret")
@GenerateAndroidIntentService(baseURL = "http://www.google.com")
public interface TestClient {

    @CommonsService(name = "sample", responseClass = SampleResponse.class)
    @POST("/test/path")
    Call<SampleResponse> sampleApiService(@Body SampleRequest sampleRequest);

    @GeneratedResponse(SampleResponse.class)
    @CommonsService(name = "service")
    @POST("/test/path")
    Call<SampleResponseApiResponse> anotherService(@Body SampleRequest sampleRequest, String param2);


    @TokenEndPoint
    @POST("/token")
    Call<AuthToken> obtainTransactionToken(@Field("grant_type") String grantType,
                                           @Field("username") String username,
                                           @Field("password")String password);

}
