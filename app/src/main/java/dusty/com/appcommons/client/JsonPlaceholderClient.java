package dusty.com.appcommons.client;

import com.dusty.commons.annotation.CommonsService;
import com.dusty.commons.annotation.GenerateRxService;
import com.dusty.commons.annotation.constants.Security;

import dusty.com.appcommons.client.dto.Photos;
import retrofit2.Call;
import retrofit2.http.GET;


@GenerateRxService(baseURL = "https://jsonplaceholder.typicode.com/")
public interface JsonPlaceholderClient {

    @CommonsService(name = "getPhotos",responseClass=Photos.class, security = Security.NONE)
    @GET("/photos")
    Call<Photos> getPhotos();
}
