package dusty.com.appcommons.client.dto;

import java.util.ArrayList;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class Photos extends ArrayList<Photos.Photo> {

    @Data
    @ToString
    public static class Photo{
        private int albumId;
        private int id;
        private String title;
        private String url;
        private String thumbnailUrl;
    }
}

