package dusty.com.appcommons.client.dto;

import com.dusty.commons.annotation.ApiResponse;

import java.io.Serializable;

/**
 * @author .: Chukwudum Ekwueme
 * email ..: chidumekwueme@gmail.com, chukwudum.ekwueme@cwlgroup.com
 * created : Mar 17, 2018 12:42 PM
 */
@ApiResponse
public class SampleResponse implements Serializable {

    private String responseCode;

}
