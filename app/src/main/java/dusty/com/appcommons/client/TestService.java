package dusty.com.appcommons.client;

import com.dusty.commons.RegisterDeviceResponseApiResponse;
import com.dusty.commons.util.AppUtil;
import com.dusty.commons.web.dto.AuthToken;
import com.dusty.commons.web.dto.CommonsResponseWrapper;
import com.dusty.commons.web.dto.TokenServiceParams;
import com.dusty.commons.web.service.TokenService;
import com.dusty.commons.web.service.util.CommonsClientHelper;
import com.dusty.commons.web.service.util.OauthUtils;

import dusty.com.appcommons.client.dto.Posts;
import dusty.com.appcommons.client.dto.RegisterDeviceRequest;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;

/**
 * @author .: Chukwudum Ekwueme
 * email ..: chidumekwueme@gmail.com, chukwudum.ekwueme@cwlgroup.com
 * created : Oct 01, 2018 9:56 AM
 */
public class TestService {

    private TokenService tokenService;

    public static Builder builder(){
        return new Builder();
    }

    public static class Builder{

        private OauthUtils oauthUtils;
        private TokenServiceParams tokenServiceParams;

        public Builder withUtils(OauthUtils oauthUtils){
            this.oauthUtils = oauthUtils;
            return this;
        }

        public Builder withTokenServiceParams(TokenServiceParams tokenServiceParams){
            this.tokenServiceParams = tokenServiceParams;
            return this;
        }

        public TestService build(){
            if(tokenServiceParams == null)
                tokenServiceParams =
                        TokenServiceParams.builder()
                                .adminClientID("")
                                .adminClientSecret("")
                                .adminScope("")
                                .adminGrantType("")
                                .adminUserName("")
                                .tokenBaseURL("")
                                .adminPassword("")
                                .userTokenClientId("")
                                .userTokenClientSecret("")
                                .userTokenGrantType("").build();


            return new TestService(oauthUtils, tokenServiceParams);
        }
    }

    public TestService(OauthUtils oauthUtils, TokenServiceParams tokenServiceParams){
        this.tokenService = new TokenService<RxClient>(RxClient.class,
                oauthUtils,
                tokenServiceParams, true ) {
            @Override
            public Call<AuthToken> obtainTransactionToken(RxClient service, String username, String password, String grantType, String scope, String clientId, String clientSecret) {
                return service.obtainTransactionToken(grantType, username, password);
            }
        };
    }

    public Single<CommonsResponseWrapper<Posts>> getPosts(String username, String password){
        return Single.fromCallable(() -> new CommonsClientHelper<RxClient, Posts>("https://jsonplaceholder.typicode.com",
                RxClient.class,username, password, false) {
            @Override
            public Call<Posts> createServiceCall(RxClient client) {
                return client.getPosts();
            }
        }.makeApiCall()).subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread());
    }


    public Single<CommonsResponseWrapper<Posts>> getPosts(){
        return Single.fromCallable(() -> new CommonsClientHelper<RxClient, Posts>("https://jsonplaceholder.typicode.com",
                RxClient.class,false) {
            @Override
            public Call<Posts> createServiceCall(RxClient client) {
                return client.getPosts();
            }
        }.makeApiCall()).subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Single<CommonsResponseWrapper<RegisterDeviceResponseApiResponse>>
    registerDevice(RegisterDeviceRequest registerDeviceRequest){
        return Single.fromCallable(() -> new CommonsClientHelper<RxClient,
                RegisterDeviceResponseApiResponse>("http://159.122.189.172",
                RxClient.class,
                AppUtil.TOKEN_TYPE_ADMIN,
                tokenService){
            public Call<RegisterDeviceResponseApiResponse> createServiceCall(RxClient client){
                return client.registerDevice(registerDeviceRequest);
            }
        }.makeApiCall()).subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread());

    }
}
