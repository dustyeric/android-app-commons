package dusty.com.appcommons.client.dto;

import java.io.Serializable;

import lombok.Builder;
import lombok.Data;

/**
 * @author .: Chukwudum Ekwueme
 * email ..: chidumekwueme@gmail.com, chukwudum.ekwueme@cwlgroup.com
 * created : Oct 01, 2018 9:53 AM
 */
@Data
@Builder
public class RegisterDeviceRequest implements Serializable {

    private String deviceType;
}
