package com.dusty.commons.processor;

import com.dusty.commons.annotation.ApiResponse;
import com.dusty.commons.annotation.GenerateAndroidIntentService;
import com.dusty.commons.annotation.GenerateBaseService;
import com.dusty.commons.annotation.TokenEndPoint;
import com.google.common.collect.ImmutableSet;
import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.ParameterizedTypeName;
import com.squareup.javapoet.TypeSpec;

import java.util.Set;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Filer;
import javax.annotation.processing.Messager;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.util.Elements;
import javax.tools.Diagnostic;

import static com.dusty.commons.processor.util.ProcessorUtil.CODE_GEN_BASE_PACKAGE;
import static com.dusty.commons.processor.util.ProcessorUtil.createType;

/**
 * @author .: Chukwudum Ekwueme
 * email ..: chidumekwueme@gmail.com, chukwudum.ekwueme@cwlgroup.com
 * created : Mar 20, 2018 10:44 AM
 * generates the token service base class
 */
public class GenerateBaseServiceProcessor extends AbstractProcessor {

    private Filer filer;
    private Messager messager;
    private Elements elements;

    private static final String BASE_TOKEN_SERVICE = "com.dusty.commons.web.service.TokenService";
    static final ClassName tokenServiceParams = ClassName.get("com.dusty.commons.web.dto", "TokenServiceParams");

    static final ClassName retrofitCallClass = ClassName.get("retrofit2", "Call");
    static final ClassName authTokenClass = ClassName.get("com.dusty.commons.web.dto", "AuthToken");
    @Override
    public synchronized void init(ProcessingEnvironment processingEnvironment) {
        super.init(processingEnvironment);
        filer = processingEnvironment.getFiler();
        messager = processingEnvironment.getMessager();
        elements = processingEnvironment.getElementUtils();
    }



    @Override
    public boolean process(Set<? extends TypeElement> set, RoundEnvironment roundEnvironment) {

        try {

            for (Element element : roundEnvironment.getElementsAnnotatedWith(GenerateBaseService.class)) {

                if (element.getKind() != ElementKind.INTERFACE) {
                    messager.printMessage(Diagnostic.Kind.ERROR, "@GenerateBaseService can only be applied to retrofit client interface");
                    return true;
                }

                TypeElement retofitAnnotatedClient = (TypeElement) element;

                GenerateBaseService annotationParams = retofitAnnotatedClient.getAnnotation(GenerateBaseService.class);

                TypeSpec.Builder baseServiceToGenerate  =  TypeSpec.classBuilder(retofitAnnotatedClient.getSimpleName()+"BaseService")
                        .addModifiers( Modifier.PUBLIC)
                        .superclass(ParameterizedTypeName.get(createType(BASE_TOKEN_SERVICE),
                                createType(retofitAnnotatedClient.getQualifiedName().toString())));


                MethodSpec constructor = MethodSpec
                        .constructorBuilder()
                        .addParameter(GenerateAndroidIntentServiceProcessor.classContext, "context")
                        .addParameter(GenerateAndroidIntentServiceProcessor.classIntent, "intent")
                        .addStatement("super(context,intent,\n" +
                                "                 $T.builder()\n" +
                                "                 .adminClientID($S)\n" +
                                "                 .adminClientSecret($S)\n" +
                                "                 .adminScope($S)\n" +
                                "                 .adminGrantType($S)\n" +
                                "                 .adminUserName($S)\n" +
                                "                 .adminPassword($S)\n" +
                                "                 .userTokenClientId($S)\n" +
                                "                 .userTokenClientSecret($S)\n" +
                                "                 .userTokenGrantType($S)\n" +
                                "\n" +
                                "                 .build(), $T.class, "+annotationParams.debugMode()+")",
                                tokenServiceParams,
                                annotationParams.adminClientId(),
                                annotationParams.adminClientSecret(),
                                annotationParams.adminScope(),
                                annotationParams.adminGrantType(),
                                annotationParams.adminUserName(),
                                annotationParams.adminPassword(),
                                annotationParams.userTokenClientId(),
                                annotationParams.userTokenClientSecret(),
                                annotationParams.userTokenGrantType(),
                                createType(retofitAnnotatedClient.getQualifiedName().toString()))
                        .build();

                baseServiceToGenerate.addMethod(constructor);

                String tokenStatement = null;


                for (Element enclosedElement : retofitAnnotatedClient.getEnclosedElements()){

                    if(enclosedElement.getKind() == ElementKind.METHOD){

                        if(enclosedElement.getAnnotation(TokenEndPoint.class) != null){

                            ExecutableElement tokenMethod = (ExecutableElement)enclosedElement;

                            tokenStatement = tokenMethod.getSimpleName().toString()+"(";

                            StringBuilder variables = new StringBuilder();

                            for(VariableElement variableElements : tokenMethod.getParameters()){

                                if(variables.length() > 1) variables.append(", ");
                                variables.append(variableElements.getSimpleName());

                            }

                            tokenStatement+=variables.toString();
                            tokenStatement+=")";


                        }

                    }

                }

                MethodSpec getTokenMethod =
                        MethodSpec.methodBuilder("obtainTransactionToken")
                                .addParameter(createType(retofitAnnotatedClient.getQualifiedName().toString()),
                                        "client")
                                .addParameter(String.class,"username")
                                .addParameter(String.class,"password")
                                .addParameter(String.class,"grantType")
                                .addParameter(String.class,"scope")
                                .addParameter(String.class,"clientId")
                                .addParameter(String.class,"clientSecret")
                                .addStatement("return client."+tokenStatement+"")
                                .addAnnotation(Override.class)
                                .returns(ParameterizedTypeName.get(retrofitCallClass, authTokenClass))
                                .addModifiers(Modifier.PUBLIC)

                        .build();

                baseServiceToGenerate.addMethod(getTokenMethod);

                //write the file
                JavaFile.builder(CODE_GEN_BASE_PACKAGE,
                        baseServiceToGenerate.build()).build().writeTo(filer);



            }







        }catch (Exception e){
            e.printStackTrace();
            messager.printMessage(Diagnostic.Kind.ERROR,
                    "Failed during base service class generation");
            messager.printMessage(Diagnostic.Kind.ERROR,
                    e.getMessage());
        }
        return true;
    }


    @Override
    public Set<String> getSupportedAnnotationTypes() {
        return ImmutableSet.of(GenerateBaseService.class.getCanonicalName());
    }

    @Override
    public SourceVersion getSupportedSourceVersion() {
        return SourceVersion.latestSupported();
    }



}
