package com.dusty.commons.processor;

import com.dusty.commons.annotation.CommonsService;
import com.dusty.commons.annotation.GenerateAndroidIntentService;
import com.dusty.commons.annotation.GenerateBaseService;
import com.dusty.commons.annotation.GeneratedResponse;
import com.dusty.commons.processor.dto.MethodSignatureDefinition;
import com.google.common.collect.ImmutableSet;
import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.FieldSpec;
import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.ParameterizedTypeName;
import com.squareup.javapoet.TypeSpec;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Filer;
import javax.annotation.processing.Messager;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.AnnotationValue;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;
import javax.tools.Diagnostic;

import static com.dusty.commons.processor.util.ProcessorUtil.CODE_GEN_BASE_PACKAGE;
import static com.dusty.commons.processor.util.ProcessorUtil.createType;
import static com.dusty.commons.processor.util.ProcessorUtil.getClassTypeMirror;

/**
 * @author .: Chukwudum Ekwueme
 * email ..: chidumekwueme@gmail.com, chukwudum.ekwueme@cwlgroup.com
 * created : Mar 17, 2018 12:23 PM
 */
public class GenerateAndroidIntentServiceProcessor extends AbstractProcessor{

    private Filer filer;
    private Messager messager;
    private Elements elements;
    private Map<String, String> clientsWithPackage;
    static final ClassName classContext = ClassName.get("android.content", "Context");
    static final ClassName classIntent = ClassName.get("android.content", "Intent");
    private static final ClassName classLoginRequest = ClassName.get("com.dusty.commons.web.dto", "UserLoginRequest");
    private static final ClassName classGenerateLoginResponse = ClassName.get("com.dusty.commons.web.dto", "GenerateLoginTokenResponse");
    private static final ClassName classBundle = ClassName.get("android.os", "Bundle");


    private static final ClassName classResultReceiver =
            ClassName.get("android.os", "ResultReceiver");

    private static final ClassName classIntentService =
            ClassName.get("android.app", "IntentService");


    private static final ClassName classRetrofitCall =
            ClassName.get("retrofit2", "Call");

    private static final ClassName classBaseActivityClass =
            ClassName.get("com.dusty.commons.activity", "BaseAppCompatActivity");

    //GenericTokenClientCallHelper
    private static final ClassName classGenericTokenClientCallHelper =
            ClassName.get("com.dusty.commons.web.service.util", "GenericTokenClientCallHelper");

    private static final ClassName classWebAPICallBackInterface =
            ClassName.get("com.dusty.commons.activity.BaseAppCompatActivity", "WebApiCallerInterface");


    private static final String OPERATION_KEY_PREFIX = "OPERATION_";
    private static final String OPERATION_VALUE_PREFIX = "operation.";
    private static final String RECEIVER_TAG = "receiverTag";

    private static final String OPERATION_LOGIN = "OPERATION_COMMONS_LOG_IN";


    private Types types;

    private final static String PARAMETER_PREFIX = "parameter_";

    @Override
    public synchronized void init(ProcessingEnvironment processingEnvironment) {
        super.init(processingEnvironment);
        filer = processingEnvironment.getFiler();
        messager = processingEnvironment.getMessager();
        elements = processingEnvironment.getElementUtils();
        clientsWithPackage = new HashMap<>();
        types = processingEnvironment.getTypeUtils();
    }

    @Override
    public Set<String> getSupportedAnnotationTypes() {
        return ImmutableSet.of(GenerateAndroidIntentService.class.getCanonicalName());
    }


    @Override
    public SourceVersion getSupportedSourceVersion() {
        return SourceVersion.latestSupported();
    }


    public String convertToUnderscoreCase(String string){

        StringBuilder convertedString = new StringBuilder();
        for(int i=0; i < string.length(); i++){
            Character c = string.charAt(i);

            if(i > 0){

                if(Character.isUpperCase(c)){

                    convertedString.append("_");
                }

            }

            convertedString.append(String.valueOf(c).toUpperCase());

        }
        return convertedString.toString();
    }







    /**
     *
     * name of method
     *
     *    public static void addNewRestaurant(Context context, ResultReceiver resultReceiver,
     AddRestaurantRequest addRestaurantRequest, String imageFilePath){




     }
     * @param clientSimpleName
     * @throws Exception
     */

    public void generateHandler(String clientSimpleName, List<MethodSignatureDefinition> methodSignatureDefinitionList) throws Exception{


        //start building the service handler class
        TypeSpec.Builder serviceHandler  = TypeSpec
                .classBuilder(clientSimpleName+"ServiceHandler")
                .superclass(classIntentService)
                .addModifiers(Modifier.PUBLIC);

        MethodSpec constructor = MethodSpec
                .constructorBuilder()
                .addModifiers(Modifier.PUBLIC)
                .addStatement("super($S)", clientSimpleName+"ServiceHandler")
                .build();


        MethodSpec onHandleIntentMethod = MethodSpec
                .methodBuilder("onHandleIntent")
                .addModifiers(Modifier.PROTECTED)
                .addParameter(classIntent, "intent")
                .beginControlFlow("if (intent != null)")
                .addStatement("final $T action = intent.getAction()", String.class)

              //  .endControlFlow()
                .build();



        //add the constructor
        serviceHandler.addMethod(constructor);
        //add on handle intent method

        int count = 0;
        for(MethodSignatureDefinition methodSignatureDefinition : methodSignatureDefinitionList){


            if(count > 0 ){
                onHandleIntentMethod = onHandleIntentMethod.toBuilder()
                        .addCode("else ")
                        .build();
            }
            count ++;
            onHandleIntentMethod = onHandleIntentMethod.toBuilder()
                        .beginControlFlow("if ($S.equals(action))",
                                methodSignatureDefinition.getOperation())
                        .addStatement("new $T(this, intent)" +
                                "."+methodSignatureDefinition.getMethodName()+"()" ,
                                methodSignatureDefinition.getServiceClass())
                        .endControlFlow()
                    .build();

            MethodSpec serviceMethod = MethodSpec
                    .methodBuilder("start"+capitalizeFirstLetter(methodSignatureDefinition.getMethodName()))
                    .addModifiers(Modifier.PUBLIC, Modifier.STATIC)
                    .addParameter(classBaseActivityClass, "commonsActivity")
                    .addParameter(ParameterizedTypeName.get(classWebAPICallBackInterface,
                            methodSignatureDefinition.getResponseClass(), methodSignatureDefinition.getErrorResponseClass()),"wepApiInterfaceCallback" )
                 //   .addParameter(classWebAPICallBackInterface, "wepApiInterfaceCallback")
                    .build();


            serviceMethod = serviceMethod.toBuilder()
                    .addStatement("$T context = ($T)commonsActivity", classContext, classContext)
                    .addStatement("$T intent = new $T(context, "+clientSimpleName+"ServiceHandler"+".class)",
                            classIntent, classIntent)
                    .addStatement("intent.putExtra($S, commonsActivity.getMReceiver())", RECEIVER_TAG)
                    .build();

            for(String key : methodSignatureDefinition.getArgs().keySet()){

                serviceMethod = serviceMethod.toBuilder()
                        .addParameter(methodSignatureDefinition.getArgs().get(key), key)
                        .addStatement("intent.putExtra($S, " +
                                key+")",
                                key+".parameter")
                        .build();
            }

            serviceMethod = serviceMethod.toBuilder()
                    .addStatement("intent.setAction($S)",
                            methodSignatureDefinition.getOperation())
                    .addStatement("commonsActivity.registerCallBack($S, wepApiInterfaceCallback)",  methodSignatureDefinition.getOperation())
                    .addStatement("context.startService(intent)")
                    .build();


            serviceHandler.addMethod(serviceMethod);
        }

        //add login method


        MethodSpec loginMethod = MethodSpec
                .methodBuilder("startCommonsUserLogin")
                .addModifiers(Modifier.PUBLIC, Modifier.STATIC)
                .addParameter(classBaseActivityClass, "commonsActivity")
                .addParameter(classWebAPICallBackInterface, "wepApiInterfaceCallback")
                .addParameter(classLoginRequest, "userLoginRequest")
                .addStatement("$T context = ($T)commonsActivity", classContext, classContext)
                .addStatement("$T intent = new $T(context, "+clientSimpleName+"ServiceHandler"+".class)",
                        classIntent, classIntent)
                .addStatement("intent.putExtra($S, commonsActivity.getMReceiver())", RECEIVER_TAG)
                .addStatement("intent.putExtra($T.USER_LOGIN_REQUEST,\n" +
                        "         userLoginRequest)", createType(CODE_GEN_BASE_PACKAGE+"." +clientSimpleName+"Service" ))
                .addStatement("intent.setAction($T."+OPERATION_LOGIN+")",
                        createType(CODE_GEN_BASE_PACKAGE+"." +clientSimpleName+"Service" ))

                .addStatement("commonsActivity.registerCallBack($T."+OPERATION_LOGIN+", wepApiInterfaceCallback)",
                        createType(CODE_GEN_BASE_PACKAGE+"." +clientSimpleName+"Service" ))
                .addStatement("context.startService(intent)")

                .build();

        onHandleIntentMethod = onHandleIntentMethod.toBuilder()
                .beginControlFlow("else if ($T."+OPERATION_LOGIN+".equals(action))",
                        createType(CODE_GEN_BASE_PACKAGE+"." +clientSimpleName+"Service" ))
                .addStatement("new $T(this, intent)" +
                                ".commonsOauthLogin()" ,
                        createType(CODE_GEN_BASE_PACKAGE+"." +clientSimpleName+"Service" ))
                .endControlFlow()
                .endControlFlow().build();

        serviceHandler.addMethod(loginMethod);
        serviceHandler.addMethod(onHandleIntentMethod);


        JavaFile.builder(CODE_GEN_BASE_PACKAGE,
                serviceHandler.build()).build().writeTo(filer);

    }

    public String capitalizeFirstLetter(String input){
        return input.substring(0, 1).toUpperCase() + input.substring(1);
    }

    private TypeSpec.Builder addLoginMethod(TypeSpec.Builder appCommonsService){

        MethodSpec loginMethod = MethodSpec
                .methodBuilder("commonsOauthLogin")
                .addModifiers(Modifier.PUBLIC)
                .addStatement("$T userLoginRequest  = ($T) intent.getSerializableExtra(USER_LOGIN_REQUEST)",
                        classLoginRequest, classLoginRequest)
                .addStatement("$T generateLoginTokenResponse =\n" +
                                "                generateLoginToken(userLoginRequest.getUsername(),\n" +
                                "                        userLoginRequest.getPassword())",
                        classGenerateLoginResponse)
                .addStatement("$T bundle = new $T()", classBundle, classBundle)
                .addStatement("bundle.putString(com.dusty.commons.web.service.util.ServiceConstants.OPERATION, "+OPERATION_LOGIN+")")

                .beginControlFlow("if(!generateLoginTokenResponse.isWasSuccessFul())")
                .addStatement("bundle.putSerializable(com.dusty.commons.web.service.util.ServiceConstants.API_ERROR, generateLoginTokenResponse.getError())")

                .addStatement("getRecieverHandle().send(com.dusty.commons.web.service.util.ServiceConstants.FAILED, bundle)")
                .endControlFlow()

                .beginControlFlow("else")
                .addStatement("getRecieverHandle().send(com.dusty.commons.web.service.util.ServiceConstants.SUCCESS, bundle)")
                .addStatement("oauthUtils.saveSuccessfulLoginCredentials(userLoginRequest.getUsername(), userLoginRequest.getPassword())")

                .endControlFlow()

                .build();

        // public static String USER_LOGIN_REQUEST = "user.token.request"
        //public static String OPERATION_LOG_IN = "operation.user.log.in";

        appCommonsService.addField(FieldSpec.builder(String.class,
                OPERATION_LOGIN)
                .initializer("$S" ,"operation.commons.user.log.in" )
                .addModifiers(Modifier.PUBLIC,Modifier.STATIC,
                        Modifier.FINAL)
                .build());

        appCommonsService.addField(FieldSpec.builder(String.class,
                "USER_LOGIN_REQUEST")
                .initializer("$S" ,"user.token.request" )
                .addModifiers(Modifier.PUBLIC,Modifier.STATIC,
                        Modifier.FINAL)
                .build());


        appCommonsService.addMethod(loginMethod);

        return  appCommonsService;
    }

    @Override
    public boolean process(Set<? extends TypeElement> set, RoundEnvironment roundEnvironment) {

        try{

            for (Element element : roundEnvironment.getElementsAnnotatedWith(GenerateAndroidIntentService.class)) {

                if (element.getKind() != ElementKind.INTERFACE) {
                    messager.printMessage(Diagnostic.Kind.ERROR, "@GenerateAndroidIntentService can only be applied to retrofit client interface");
                    return true;
                }

                TypeElement annotatedClientType = (TypeElement) element;

                String DEF_API_BASE_URL = element.getAnnotation(GenerateAndroidIntentService.class)
                        .baseURL();


                //get the qualified name for the base service class
                String baseClassFullQualifiedName = "";

                //if a base class as generated use that instead
                if(element.getAnnotation(GenerateBaseService.class) != null){

                    baseClassFullQualifiedName = CODE_GEN_BASE_PACKAGE + "." + element.getSimpleName() + "BaseService";

                }else{
                    baseClassFullQualifiedName =  ((TypeElement)types.asElement(getClassTypeMirror(GenerateAndroidIntentService.class,
                            annotatedClientType, "baseServiceClass"))).getQualifiedName().toString();
                }

                //start building the service handler class
                TypeSpec.Builder appCommonsService  = TypeSpec
                        .classBuilder(element.getSimpleName()+"Service")
                        .superclass(createType(baseClassFullQualifiedName))
                        .addModifiers(Modifier.PUBLIC);

                MethodSpec constructor = MethodSpec
                        .constructorBuilder()
                        .addModifiers(Modifier.PUBLIC)
                        .addParameter(classContext, "context")
                        .addParameter(classIntent, "intent")
                        .addStatement("super(context, intent)")
                        .build();


                appCommonsService.addMethod(constructor);

                HashMap<String, FieldSpec> fieldSpecs = new HashMap<>();


                HashMap<String, MethodSpec> serviceNames = new HashMap<>();

                List<MethodSignatureDefinition> methodSignatureDefinitions =
                        new ArrayList<>();

                //for every api that has the annotation commons service
                for(Element enclosedElement : annotatedClientType.getEnclosedElements()){

                    if(enclosedElement.getKind() == ElementKind.METHOD){

                        if(enclosedElement.getAnnotation(CommonsService.class) == null){
                            continue;
                        }

                        String serviceName =
                                enclosedElement.getAnnotation(CommonsService.class).name();

                        String tokenType =
                                enclosedElement.getAnnotation(CommonsService.class).tokenType();

                        String serviceBaseUrl = enclosedElement.getAnnotation(CommonsService.class).baseURL();
                        if("".equalsIgnoreCase(serviceBaseUrl)){
                            serviceBaseUrl = DEF_API_BASE_URL;
                        }

                        //first check if a generated response exists

                        ClassName returnType;

                        if(enclosedElement.getAnnotation(GeneratedResponse.class) != null){


                            TypeElement returnTypeElement =
                                    (TypeElement) types.asElement(getClassTypeMirror(GeneratedResponse.class,
                                            enclosedElement , "value"));
                            returnType =
                                    createType(CODE_GEN_BASE_PACKAGE + "." +
                                    returnTypeElement.getSimpleName()+"ApiResponse");

                        }else{

                            //throw error if no response class was specified
                            TypeElement returnTypeElement =
                                    (TypeElement) types.asElement(getClassTypeMirror(CommonsService.class,
                                            enclosedElement , "responseClass"));

                            returnType = createType(returnTypeElement.getQualifiedName()
                                    .toString());

                        }



                        if(serviceNames.containsKey(serviceName)){
                            messager.printMessage(Diagnostic.Kind.ERROR,
                                    "duplicate service name : " + serviceName);
                        }

                        ExecutableElement executableElement =
                                (ExecutableElement)enclosedElement;


                        //generate the equivalent service method spec
                        MethodSpec serviceMethod = MethodSpec
                                .methodBuilder(serviceName)
                                .addModifiers(Modifier.PUBLIC)
                                .build();

                        //for all the required arguments of the service
                        StringBuilder arguments = new StringBuilder();

                        //list of all the required additional  arguments
                        HashMap<String, ClassName> methodArgs = new HashMap<>();

                        for(VariableElement variableElement : executableElement.getParameters()){


                            if(arguments.length() > 0){
                                arguments.append(",").append(variableElement.getSimpleName());

                            }else{
                                arguments.append(variableElement.getSimpleName());
                            }

                            TypeMirror typeMirror = variableElement.asType();
                            TypeElement fieldElement = (TypeElement)types.asElement(typeMirror);

                            //add argument to the list
                            methodArgs.put(variableElement.getSimpleName().toString(),
                                    createType(fieldElement.getQualifiedName().toString()));


                            String fieldKey = convertToUnderscoreCase(PARAMETER_PREFIX + variableElement.getSimpleName());
                            //create the field parameter keys

                            FieldSpec operation = FieldSpec.builder(String.class,
                                    fieldKey)
                                    .initializer("$S" ,variableElement.getSimpleName().toString() + ".parameter" )
                                    .addModifiers(Modifier.PUBLIC,Modifier.STATIC,
                                            Modifier.FINAL)
                                    .build();


                            fieldSpecs.put(variableElement.getSimpleName().toString(), operation);


                            //generate the input for the client service
                            serviceMethod = serviceMethod.toBuilder()
                                    .addStatement("final $T "+variableElement.getSimpleName()+" = ($T)\n" +
                                            "                 intent.getSerializableExtra("+fieldKey+")",
                                            createType(fieldElement.getQualifiedName()
                                                    .toString()),
                                            createType(fieldElement.getQualifiedName()
                                                    .toString()) )
                                    .build();



                        }




                        serviceMethod = serviceMethod
                                .toBuilder()
                                .addStatement("new $T<$T, $T>(this, $S\n" +
                                        "                 , $S, "+annotatedClientType.getSimpleName()+".class) {\n" +
                                        "                @Override\n" +
                                        "                public $T<$T> createServiceCall($T client) {\n" +
                                        "                return client."+executableElement.getSimpleName()+"("+arguments.toString()+");\n" +
                                        "                }\n" +
                                        "                }.makeCall()", classGenericTokenClientCallHelper,
                                        returnType,
                                        createType(annotatedClientType.getQualifiedName().toString()),
                                        serviceBaseUrl, tokenType,
                                        classRetrofitCall,
                                        returnType,
                                        createType(annotatedClientType.getQualifiedName().toString())

                                        )
                                .build();




                        String operationKey = OPERATION_KEY_PREFIX +  convertToUnderscoreCase(serviceName);
                        String operationValue =  operationKey;


                        FieldSpec operation = FieldSpec.builder(String.class,
                                operationKey)
                                .initializer("$S" ,operationValue)
                                .addModifiers(Modifier.PUBLIC,Modifier.STATIC,
                                        Modifier.FINAL)
                                .build();


                        fieldSpecs.put(operationKey, operation);

                        serviceNames.put(serviceName, serviceMethod);

                        //TODO: when generating the service class add the types here

                        //check if the error response class is the same as the object class here
                        ClassName errorResponseClass = null;
                        if(!enclosedElement.getAnnotation(CommonsService.class).unifiedResponse()){

                            TypeElement errorClassTypeElement =
                                    (TypeElement) types.asElement(getClassTypeMirror(CommonsService.class,
                                            enclosedElement , "errorClass"));

                            errorResponseClass = createType(errorClassTypeElement.getQualifiedName()
                                    .toString());
                        }

                        methodSignatureDefinitions.add(new
                                MethodSignatureDefinition(serviceName,
                                methodArgs, operationValue,
                                createType(CODE_GEN_BASE_PACKAGE +
                                        "." + capitalizeFirstLetter(element.getSimpleName()+"Service")),
                                returnType, errorResponseClass));


                    }

                }


                addLoginMethod(appCommonsService);



                appCommonsService.addFields(fieldSpecs.values());
                appCommonsService.addMethods(serviceNames.values());


                //create the commons file here
                JavaFile.builder(CODE_GEN_BASE_PACKAGE,
                        appCommonsService.build()).build().writeTo(filer);

                //generate the handler service that will be used to make requests
                generateHandler(annotatedClientType.getSimpleName().toString(), methodSignatureDefinitions);

                clientsWithPackage.put(
                        annotatedClientType.getSimpleName().toString(),
                        elements.getPackageOf(annotatedClientType).getQualifiedName().toString());



            }



        }catch (Exception e){
            e.printStackTrace();
            messager.printMessage(Diagnostic.Kind.ERROR,
                    "Failed during intent service class generation");
            if(e.getMessage() != null){
                messager.printMessage(Diagnostic.Kind.ERROR,
                        e.getMessage());
            }


        }
        return true;
    }
}
