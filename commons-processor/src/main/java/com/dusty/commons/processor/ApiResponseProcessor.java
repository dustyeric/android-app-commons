package com.dusty.commons.processor;

import com.dusty.commons.annotation.ApiResponse;
import com.google.common.collect.ImmutableSet;
import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.ParameterizedTypeName;
import com.squareup.javapoet.TypeSpec;

import java.io.Serializable;
import java.util.Set;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Filer;
import javax.annotation.processing.Messager;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.lang.model.util.Elements;
import javax.tools.Diagnostic;

import static com.dusty.commons.processor.util.ProcessorUtil.CODE_GEN_BASE_PACKAGE;
import static com.dusty.commons.processor.util.ProcessorUtil.createType;

/**
 * @author .: Chukwudum Ekwueme
 * email ..: chidumekwueme@gmail.com, chukwudum.ekwueme@cwlgroup.com
 * created : Mar 18, 2018 5:00 AM
 */
public class ApiResponseProcessor  extends AbstractProcessor {

    private Filer filer;
    private Messager messager;
    private Elements elements;

    private static final String BASE_API_RESPONSE_CLASS = "com.dusty.commons.web.dto.ApiResponse";
    private static final String BASE_RESPONSE_ENTITY_CLASS = "com.dusty.commons.web.dto.ResponseEntity";

    private static final String LOMBOK_DATA_ANNOTATION = "lombok.Data";
    @Override
    public synchronized void init(ProcessingEnvironment processingEnvironment) {
        super.init(processingEnvironment);
        filer = processingEnvironment.getFiler();
        messager = processingEnvironment.getMessager();
        elements = processingEnvironment.getElementUtils();
    }



    @Override
    public boolean process(Set<? extends TypeElement> set, RoundEnvironment roundEnvironment) {

        try {


            for (Element element : roundEnvironment.getElementsAnnotatedWith(ApiResponse.class)) {


                if (element.getKind() != ElementKind.CLASS) {
                    messager.printMessage(Diagnostic.Kind.ERROR, "@ApiResponse can only be applied to a java pojo class");
                    return true;
                }

                TypeElement annotatedClientType = (TypeElement) element;


                ClassName originalClass = createType(annotatedClientType.getQualifiedName().toString());

                ClassName responseEntity = ClassName.get(CODE_GEN_BASE_PACKAGE,
                        element.getSimpleName()+"ApiResponse",
                        "ResponseEntity");

                //start building the api response class
                TypeSpec.Builder generatedApiResponse  = TypeSpec
                        .classBuilder(element.getSimpleName()+"ApiResponse")
                        .superclass(createType(BASE_API_RESPONSE_CLASS))
                        .addField(responseEntity, "responseEntity", Modifier.PRIVATE)
                        .addSuperinterface(Serializable.class)
                        .addModifiers(Modifier.PUBLIC)

                        //create the inner class
                                .addType(TypeSpec.classBuilder(responseEntity.simpleName())
                                        .addModifiers(Modifier.STATIC, Modifier.PUBLIC)
                                        .superclass(ParameterizedTypeName.get(createType(BASE_RESPONSE_ENTITY_CLASS),
                                                originalClass))
                                      //  .superclass(createType(BASE_RESPONSE_ENTITY_CLASS))
                                        .build());

               // generatedApiResponse.addS

                /**
                 *  public boolean canEqual(Object other) {
                 return other instanceof AddRestaurantResponse;
                 }
                 */

                MethodSpec canEqualsTo = MethodSpec
                        .methodBuilder("canEqual")
                        .returns(boolean.class)
                        .addModifiers(Modifier.PUBLIC)
                        .addParameter(Object.class, "other")
                        .addStatement("return other instanceof "+element.getSimpleName()+"ApiResponse")
                        .build();


                generatedApiResponse.addMethod(canEqualsTo);

                generatedApiResponse.addAnnotation(createType(LOMBOK_DATA_ANNOTATION));


                //create the commons file here
                JavaFile.builder(CODE_GEN_BASE_PACKAGE,
                        generatedApiResponse.build()).build().writeTo(filer);

            }




        }catch (Exception e){
            e.printStackTrace();
            messager.printMessage(Diagnostic.Kind.ERROR,
                    e.getMessage());

            messager.printMessage(Diagnostic.Kind.ERROR,
                    "Failed during base api response");
        }




        return true;
    }


    @Override
    public Set<String> getSupportedAnnotationTypes() {
        return ImmutableSet.of(ApiResponse.class.getCanonicalName());
    }

    @Override
    public SourceVersion getSupportedSourceVersion() {
        return SourceVersion.latestSupported();
    }




}
