package com.dusty.commons.processor;

import com.dusty.commons.annotation.CommonsService;
import com.dusty.commons.annotation.GenerateAndroidIntentService;
import com.dusty.commons.annotation.GenerateBaseService;
import com.dusty.commons.annotation.GenerateRxService;
import com.dusty.commons.annotation.GeneratedResponse;
import com.dusty.commons.annotation.TokenEndPoint;
import com.dusty.commons.annotation.constants.Security;
import com.dusty.commons.processor.util.ProcessorUtil;
import com.google.common.collect.ImmutableSet;
import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.FieldSpec;
import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.ParameterSpec;
import com.squareup.javapoet.ParameterizedTypeName;
import com.squareup.javapoet.TypeName;
import com.squareup.javapoet.TypeSpec;

import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Filer;
import javax.annotation.processing.Messager;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;
import javax.tools.Diagnostic;

import static com.dusty.commons.processor.util.ProcessorUtil.CODE_GEN_BASE_PACKAGE;
import static com.dusty.commons.processor.util.ProcessorUtil.createType;
import static com.dusty.commons.processor.util.ProcessorUtil.getClassTypeMirror;

/**
 * @author .: Chukwudum Ekwueme
 * email ..: chidumekwueme@gmail.com, chukwudum.ekwueme@cwlgroup.com
 * created : Oct 01, 2018 12:38 PM
 */
public class GenerateRxServiceProcessor extends AbstractProcessor {

    private Filer filer;
    private Messager messager;
    private Elements elements;
    private Map<String, String> clientsWithPackage;
    private Types types;

    private static final ClassName oauthUtilsClass = ClassName.get("com.dusty.commons.web.service.util", "OauthUtils");
    private static final ClassName tokenServiceParamsClass = ClassName.get("com.dusty.commons.web.dto", "TokenServiceParams");
    private static final ClassName tokenServiceClass = ClassName.get("com.dusty.commons.web.service", "TokenService");
    private static final ClassName authTokenClass = ClassName.get("com.dusty.commons.web.dto", "AuthToken");
    private static final ClassName classRetrofitCall = ClassName.get("retrofit2", "Call");
    private static final ClassName commonsResponseWrapper = ClassName.get("com.dusty.commons.web.dto", "CommonsResponseWrapper");
    private static final ClassName rxSingleClass = ClassName.get("io.reactivex", "Single");
    private static final ClassName commonsClientHelperClass = ClassName.get("com.dusty.commons.web.service.util", "CommonsClientHelper");
    private static final ClassName androidSchedulersClass = ClassName.get("io.reactivex.android.schedulers", "AndroidSchedulers");
    private static final ClassName schedulersClass = ClassName.get("io.reactivex.schedulers", "Schedulers");



    @Override
    public synchronized void init(ProcessingEnvironment processingEnvironment) {
        super.init(processingEnvironment);
        filer = processingEnvironment.getFiler();
        messager = processingEnvironment.getMessager();
        elements = processingEnvironment.getElementUtils();
        clientsWithPackage = new HashMap<>();
        types = processingEnvironment.getTypeUtils();
    }

    @Override
    public Set<String> getSupportedAnnotationTypes() {
        return ImmutableSet.of(GenerateRxService.class.getCanonicalName());
    }


    @Override
    public SourceVersion getSupportedSourceVersion() {
        return SourceVersion.latestSupported();
    }




    @Override
    public boolean process(Set<? extends TypeElement> set, RoundEnvironment roundEnvironment) {

        try{

            for (Element element : roundEnvironment.getElementsAnnotatedWith(GenerateRxService.class)) {

                if (element.getKind() != ElementKind.INTERFACE) {
                    messager.printMessage(Diagnostic.Kind.ERROR, "@GenerateRxService can only be applied to retrofit client interface");
                    return true;
                }
                //write the file
                JavaFile.builder(CODE_GEN_BASE_PACKAGE,
                        generateRxService(element).build()).build().writeTo(filer);
            }

        }catch (Exception e){
            e.printStackTrace();
            messager.printMessage(Diagnostic.Kind.ERROR,
                    "Failed during rx service class generation");
            messager.printMessage(Diagnostic.Kind.ERROR,
                    e.getMessage());
        }
        return false;
    }

    private TypeSpec generateServiceBuilder(String generatedServiceName){

        MethodSpec withUtilsBuilderMethod = MethodSpec.methodBuilder("withUtils")
                .addModifiers(Modifier.PUBLIC)
                .addParameter(oauthUtilsClass, "oauthUtils")
                .returns(ClassName.get("","Builder"))
                .addStatement("this.oauthUtils = oauthUtils")
                .addStatement("return this")
                .build();


        MethodSpec withTokenServiceParamsMethod = MethodSpec.methodBuilder("withTokenServiceParams")
                .addModifiers(Modifier.PUBLIC)
                .addParameter(tokenServiceParamsClass, "tokenServiceParams")
                .returns(ClassName.get("","Builder"))
                .addStatement("this.tokenServiceParams = tokenServiceParams")
                .addStatement("return this")
                .build();

        MethodSpec buildMethod = MethodSpec.methodBuilder("build")
                .addModifiers(Modifier.PUBLIC)
                .addStatement("if(tokenServiceParams == null)\n" +
                        "                         tokenServiceParams =\n" +
                        "                                  TokenServiceParams.builder()\n" +
                        "                                          .adminClientID(\"\")\n" +
                        "                                          .adminClientSecret(\"\")\n" +
                        "                                          .adminScope(\"\")\n" +
                        "                                          .adminGrantType(\"\")\n" +
                        "                                          .adminUserName(\"\")\n" +
                        "                                          .tokenBaseURL(\"\")\n" +
                        "                                          .adminPassword(\"\")\n" +
                        "                                          .userTokenClientId(\"\")\n" +
                        "                                          .userTokenClientSecret(\"\")\n" +
                        "                                          .userTokenGrantType(\"\").build()")
                .addStatement("return new $T(oauthUtils, tokenServiceParams)", ClassName.get("", generatedServiceName))
                .returns(ClassName.get("", generatedServiceName))
                .build();

        return TypeSpec.classBuilder("Builder")
               .addModifiers(Modifier.PUBLIC, Modifier.STATIC)
                .addField(oauthUtilsClass, "oauthUtils", Modifier.PRIVATE)
                .addField(tokenServiceParamsClass, "tokenServiceParams", Modifier.PRIVATE)
                .addMethod(withUtilsBuilderMethod)
                .addMethod(withTokenServiceParamsMethod)
                .addMethod(buildMethod)
                .build();

    }

    private MethodSpec generateRxServiceConstructor(TypeElement annotatedClientType, boolean hasTokenEndpoint){

       ClassName clientClass = createType(annotatedClientType.getQualifiedName().toString());

        MethodSpec.Builder builder = MethodSpec.constructorBuilder()
                .addModifiers(Modifier.PUBLIC)
                .addParameter(oauthUtilsClass, "oauthUtils")
                .addParameter(tokenServiceParamsClass, "tokenServiceParams");


        String obtainTokenStatement = hasTokenEndpoint? "return service.obtainTransactionToken(grantType, username, password);" : "return null;";

        builder.addStatement("this.tokenService = new $T<$T>($T.class,\n" +
                        "                oauthUtils,\n" +
                        "                tokenServiceParams, true ) {\n" +
                        "            @Override\n" +
                        "            public $T<$T> obtainTransactionToken($T service, String username, String password, String grantType, String scope, String clientId, String clientSecret) {\n" +
                        "                "+obtainTokenStatement+"\n" +
                        "            }\n" +
                        "        }", tokenServiceClass, clientClass,clientClass,
                classRetrofitCall,  authTokenClass, clientClass);






        return builder.build();

    }

    private boolean doesTypeHaveTokenEndPoint(Element element){
        boolean foundTokenEndpoint = false;
        for(Element enclosedElement : element.getEnclosedElements()) {
            if (enclosedElement.getKind() == ElementKind.METHOD) {
                if (enclosedElement.getAnnotation(TokenEndPoint.class) != null) {
                    foundTokenEndpoint = true;
                    break;
                }
            }
        }

        return foundTokenEndpoint;
    }

    private ClassName deduceResponseType(Element element){
        ClassName returnType;

        //check if return type was specified
        if(element.getAnnotation(GeneratedResponse.class) != null){
            TypeElement returnTypeElement =
                    (TypeElement) types.asElement(getClassTypeMirror(GeneratedResponse.class,
                            element , "value"));
            returnType =
                    createType(CODE_GEN_BASE_PACKAGE + "." +
                            returnTypeElement.getSimpleName()+"ApiResponse");

        }else{

            try {
                //throw error if no response class was specified
                TypeElement returnTypeElement =
                        (TypeElement) types.asElement(getClassTypeMirror(CommonsService.class,
                                element , "responseClass"));

                returnType = createType(returnTypeElement.getQualifiedName()
                        .toString());

            }catch (Exception e){
                throw new RuntimeException("please specify a return type for the service " +element.getSimpleName());
            }

        }


        return returnType;
    }


    private List<ParameterSpec> generateServiceMethodParameters(Element element){

        List<ParameterSpec> parameterSpecs = new ArrayList<>();
        ExecutableElement executableElement = (ExecutableElement)element;
        for(VariableElement variableElement : executableElement.getParameters()) {

            TypeMirror typeMirror = variableElement.asType();
            if(typeMirror.getKind().isPrimitive()){
                TypeName typeName = TypeName.get(typeMirror);
                parameterSpecs.add(ParameterSpec.builder(typeName,
                        variableElement.getSimpleName().toString())
                        .build());
            }else{

                TypeElement fieldElement = (TypeElement) types.asElement(typeMirror);
                parameterSpecs.add(ParameterSpec.builder(createType(fieldElement.getQualifiedName().toString()),
                        variableElement.getSimpleName().toString())
                        .build());

            }


        }

        return parameterSpecs;
    }

    private String getParamVariableNames(List<ParameterSpec> parameterSpecs){
        StringBuilder params = new StringBuilder();
        for(int i =0; i <parameterSpecs.size(); i++){
            params.append(parameterSpecs.get(i).name);
            if(i+1 < parameterSpecs.size())
                params.append(",");
        }
        return params.toString();
    }

    private MethodSpec generateBasicAPIMethod(Element element, String serviceName,ParameterizedTypeName wrappedReturnType, String serviceBaseUrl,
                                           ClassName clientClass,ClassName returnType, boolean debug){

        List<ParameterSpec> parameterSpecs = Arrays.asList(ParameterSpec.builder(String.class, "username").build(),
                ParameterSpec.builder(String.class, "password").build());

        List<ParameterSpec> mainParameters =  generateServiceMethodParameters(element);
        parameterSpecs.addAll(mainParameters);

        //generate the equivalent service method spec
        MethodSpec.Builder serviceMethod = MethodSpec.methodBuilder(serviceName)
                .addModifiers(Modifier.PUBLIC)
                .returns(wrappedReturnType);
        serviceMethod.addParameters(parameterSpecs);

        serviceMethod.addStatement("return Single.fromCallable(() -> new $T<$T, $T>($S,\n" +
                        "                $T.class, username, password, $L) {\n" +
                        "            @Override\n" +
                        "            public Call<$T> createServiceCall($T client) {\n" +
                        "                return client."+element.getSimpleName().toString()+"("+getParamVariableNames(mainParameters)+");\n" +
                        "            }\n" +
                        "        }.makeApiCall()).subscribeOn($T.computation())\n" +
                        "                .observeOn($T.mainThread())", commonsClientHelperClass, clientClass, returnType, serviceBaseUrl,
                clientClass,debug,
                returnType, clientClass, schedulersClass, androidSchedulersClass);

        return serviceMethod.build();

    }

    private MethodSpec generatePlainAPIMethod(Element element, String serviceName,ParameterizedTypeName wrappedReturnType, String serviceBaseUrl,
                                              ClassName clientClass,ClassName returnType, boolean debug ){

        List<ParameterSpec> parameterSpecs = generateServiceMethodParameters(element);
        //generate the equivalent service method spec
        MethodSpec.Builder serviceMethod = MethodSpec.methodBuilder(serviceName)
                .addModifiers(Modifier.PUBLIC)
                .returns(wrappedReturnType);
        serviceMethod.addParameters(parameterSpecs);

        serviceMethod.addStatement("return Single.fromCallable(() -> new $T<$T, $T>($S,\n" +
                "                $T.class,$L) {\n" +
                "            @Override\n" +
                "            public Call<$T> createServiceCall($T client) {\n" +
                        "                return client."+element.getSimpleName().toString()+"("+getParamVariableNames(parameterSpecs)+");\n" +
                "            }\n" +
                "        }.makeApiCall()).subscribeOn($T.computation())\n" +
                "                .observeOn($T.mainThread())", commonsClientHelperClass, clientClass, returnType, serviceBaseUrl,
                clientClass,debug,
                returnType, clientClass, schedulersClass, androidSchedulersClass);

        return serviceMethod.build();
    }

    private MethodSpec generateOauthAPIMethod(Element element, String serviceName, ParameterizedTypeName wrappedReturnType, String serviceBaseUrl,
                                              ClassName clientClass, String tokenType, ClassName returnType){

        List<ParameterSpec> parameterSpecs = generateServiceMethodParameters(element);

        //generate the equivalent service method spec
        MethodSpec.Builder serviceMethod = MethodSpec.methodBuilder(serviceName)
                .addModifiers(Modifier.PUBLIC)
                .returns(wrappedReturnType);
        serviceMethod.addParameters(parameterSpecs);

        serviceMethod.addStatement("return Single.fromCallable(() -> new $T<$T,\n" +
                        "                $T>($S,\n" +
                        "                $T.class,\n" +
                        "                $S,\n" +
                        "                tokenService){\n" +
                        "            public Call<$T> createServiceCall($T client){\n" +

                        "                return client."+element.getSimpleName().toString()+"("+getParamVariableNames(parameterSpecs)+");\n" +
                        "            }\n" +
                        "        }.makeApiCall()).subscribeOn($T.computation())\n" +
                        "                .observeOn($T.mainThread())", commonsClientHelperClass, clientClass, returnType,
                serviceBaseUrl,clientClass,tokenType,returnType, clientClass, schedulersClass, androidSchedulersClass);

        return serviceMethod.build();
    }

    private MethodSpec generateApiMethod(Element element, TypeElement annotatedClientType, String baseURL){

        ClassName clientClass = createType(annotatedClientType.getQualifiedName().toString());

        String serviceName = element.getAnnotation(CommonsService.class).name().equals("") ? element.getSimpleName().toString() : element.getAnnotation(CommonsService.class).name();
        String tokenType = element.getAnnotation(CommonsService.class).tokenType();
        String serviceBaseUrl = "".equalsIgnoreCase(element.getAnnotation(CommonsService.class).baseURL())? baseURL :
                element.getAnnotation(CommonsService.class).baseURL();
        Security security = element.getAnnotation(CommonsService.class).security();
        boolean debug = element.getAnnotation(CommonsService.class).debug();


        ClassName returnType = deduceResponseType(element);
        ParameterizedTypeName wrappedReturnType = ParameterizedTypeName.get(rxSingleClass, ParameterizedTypeName.get(commonsResponseWrapper, returnType));

        MethodSpec methodSpec = null;
        //TODO: if no name use the name of the method

        switch (security){
            case BASIC:
                methodSpec = generateBasicAPIMethod(element,serviceName,wrappedReturnType,serviceBaseUrl,clientClass,returnType, debug);
                break;

            case OAUTH:
                methodSpec = generateOauthAPIMethod(element, serviceName, wrappedReturnType, serviceBaseUrl, clientClass, tokenType, returnType);
                break;

            case NONE:
                methodSpec = generatePlainAPIMethod(element,serviceName,wrappedReturnType,serviceBaseUrl,clientClass,returnType, debug);
                break;
        }

        return methodSpec;
    }




    private List<MethodSpec> generateApiMethodCalls(Element element, TypeElement annotatedClientType, String baseURL){

        List<MethodSpec> methodSpecs = new ArrayList<>();
        for(Element enclosedElement : element.getEnclosedElements()){
            if(enclosedElement.getKind() == ElementKind.METHOD) {
                if(enclosedElement.getAnnotation(CommonsService.class) == null){
                    continue;
                }
                methodSpecs.add(generateApiMethod(enclosedElement, annotatedClientType, baseURL));
            }
        }
        return methodSpecs;
    }

    private MethodSpec generateBuilderMethod(){
        return MethodSpec.methodBuilder("builder")
                .addModifiers(Modifier.PUBLIC, Modifier.STATIC)
                .returns(ClassName.get("", "Builder"))
                .addStatement("return new Builder()")
                .build();
    }

    private TypeSpec.Builder generateRxService(Element element) throws Exception{

        TypeElement annotatedClientType = (TypeElement) element;
        //get the base url
        String baseURL = element.getAnnotation(GenerateRxService.class)
                .baseURL();

        TypeSpec.Builder serviceToGenerate  =
                TypeSpec.classBuilder(annotatedClientType.getSimpleName()+"RxService")
                .addModifiers(Modifier.PUBLIC);

        //add the builder
        serviceToGenerate.addField(tokenServiceClass, "tokenService", Modifier.PRIVATE);
        serviceToGenerate.addType(generateServiceBuilder(annotatedClientType.getSimpleName()+"RxService"));
        serviceToGenerate.addMethod(generateRxServiceConstructor(annotatedClientType, doesTypeHaveTokenEndPoint(element)));
        serviceToGenerate.addMethods(generateApiMethodCalls(element, annotatedClientType,baseURL));
        serviceToGenerate.addMethod(generateBuilderMethod());
        return serviceToGenerate;


    }

}
