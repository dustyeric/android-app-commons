package com.dusty.commons.processor.util;

import com.squareup.javapoet.ClassName;

import java.util.Map;

import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.AnnotationValue;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.type.TypeMirror;

/**
 * @author .: Chukwudum Ekwueme
 * email ..: chidumekwueme@gmail.com, chukwudum.ekwueme@cwlgroup.com
 * created : Mar 18, 2018 5:09 AM
 */
public class ProcessorUtil {


    public static final String CODE_GEN_BASE_PACKAGE = "com.dusty.commons";



    private static AnnotationMirror getAnnotationMirror(Element typeElement, Class<?> clazz) {
        String clazzName = clazz.getName();
        for(AnnotationMirror m : typeElement.getAnnotationMirrors()) {
            if(m.getAnnotationType().toString().equals(clazzName)) {
                return m;
            }
        }
        return null;
    }

    private static AnnotationValue getAnnotationValue(AnnotationMirror annotationMirror, String key) {
        for(Map.Entry<? extends ExecutableElement, ? extends AnnotationValue> entry : annotationMirror.getElementValues().entrySet() ) {
            if(entry.getKey().getSimpleName().toString().equals(key)) {
                return entry.getValue();
            }
        }
        return null;
    }


    public static TypeMirror getClassTypeMirror(Class annotationClazz, Element foo, String key) {
        AnnotationMirror am = getAnnotationMirror(foo, annotationClazz);
        if(am == null) {
            return null;
        }
        AnnotationValue av = getAnnotationValue(am, key);
        if(av == null) {
            return null;
        } else {
            return (TypeMirror)av.getValue();
        }
    }


    public static ClassName createType(String fullQualifiedClassName){

        int lastIndex =  fullQualifiedClassName.lastIndexOf(".");

        String baseServicePackageName = fullQualifiedClassName.substring(0,lastIndex );
        String baseServiceSimpleName = fullQualifiedClassName.substring(lastIndex + 1, fullQualifiedClassName.length());

        return
                ClassName.get(baseServicePackageName,
                        baseServiceSimpleName);

    }

}
