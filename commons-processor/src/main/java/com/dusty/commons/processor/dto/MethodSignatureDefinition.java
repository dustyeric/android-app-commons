package com.dusty.commons.processor.dto;

import com.squareup.javapoet.ClassName;

import java.io.Serializable;
import java.util.HashMap;

/**
 * @author .: Chukwudum Ekwueme
 * email ..: chidumekwueme@gmail.com, chukwudum.ekwueme@cwlgroup.com
 * created : Mar 17, 2018 5:51 PM
 */


public class MethodSignatureDefinition implements Serializable {

    private String methodName;

    private HashMap<String,ClassName> args;

    private ClassName responseClass;

    private ClassName errorResponseClass;

    private String operation;

    private ClassName serviceClass;


    public ClassName getResponseClass() {
        return responseClass;
    }

    public void setResponseClass(ClassName responseClass) {
        this.responseClass = responseClass;
    }

    public ClassName getErrorResponseClass() {
        return errorResponseClass;
    }

    public MethodSignatureDefinition(String methodName, HashMap<String, ClassName> args, String operation, ClassName serviceClass, ClassName responseClass, ClassName errorResponseClass) {
        this.methodName = methodName;
        this.args = args;
        this.operation = operation;
        this.serviceClass = serviceClass;
        this.responseClass = responseClass;
        if(errorResponseClass == null){
            this.errorResponseClass = responseClass;
        }else this.errorResponseClass = errorResponseClass;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public HashMap<String, ClassName> getArgs() {
        return args;
    }

    public void setArgs(HashMap<String, ClassName> args) {
        this.args = args;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public ClassName getServiceClass() {
        return serviceClass;
    }

    public void setServiceClass(ClassName serviceClass) {
        this.serviceClass = serviceClass;
    }
}
