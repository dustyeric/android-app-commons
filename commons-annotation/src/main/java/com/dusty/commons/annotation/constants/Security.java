package com.dusty.commons.annotation.constants;

public enum Security {
    NONE, OAUTH, BASIC
}
