package com.dusty.commons.annotation;

/**
 * @author .: Chukwudum Ekwueme
 * email ..: chidumekwueme@gmail.com, chukwudum.ekwueme@cwlgroup.com
 * created : Mar 20, 2018 10:43 AM
 */
public @interface TokenEndPoint {
}
