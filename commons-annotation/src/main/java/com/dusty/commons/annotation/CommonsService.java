package com.dusty.commons.annotation;

import com.dusty.commons.annotation.constants.Security;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author .: Chukwudum Ekwueme
 * email ..: chidumekwueme@gmail.com, chukwudum.ekwueme@cwlgroup.com
 * created : Mar 17, 2018 2:09 PM
 */

@Retention(RetentionPolicy.SOURCE)
@Target(ElementType.METHOD)
public @interface CommonsService {
    String name() default "";
    String baseURL() default "";
    boolean debug() default false;
    Class  responseClass() default Object.class;
    boolean unifiedResponse()default true;
    Class errorClass() default Object.class;
    String tokenType() default "token.type.user";
    Security security() default Security.OAUTH;

}
