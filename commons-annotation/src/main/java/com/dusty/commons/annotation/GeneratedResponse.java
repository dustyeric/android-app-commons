package com.dusty.commons.annotation;

/**
 * @author .: Chukwudum Ekwueme
 * email ..: chidumekwueme@gmail.com, chukwudum.ekwueme@cwlgroup.com
 * created : Mar 20, 2018 9:38 AM
 */
public @interface GeneratedResponse {

    Class value();
}
