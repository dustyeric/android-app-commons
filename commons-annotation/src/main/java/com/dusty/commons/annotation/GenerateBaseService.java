package com.dusty.commons.annotation;

/**
 * @author .: Chukwudum Ekwueme
 * email ..: chidumekwueme@gmail.com, chukwudum.ekwueme@cwlgroup.com
 * created : Mar 20, 2018 10:32 AM
 */
public @interface GenerateBaseService {

    String adminClientId() default "";
    String adminClientSecret() default "";
    String adminScope() default "";
    String tokenApiBaseURL() default "";
    String adminGrantType() default "client_credentials";
    String adminUserName() default "";
    String adminPassword() default "";
    String userTokenClientId() default "";
    String userTokenClientSecret() default "";
    String userTokenGrantType() default "password";
    boolean debugMode() default false;



}
