package com.dusty.commons.util;

import android.util.Log;

/**
 * @author .: Oriaghan Uyi
 * email ..: oriaghan3@gmail.com, uyi.oriaghan@cwg-plc.com
 * created : 2/5/18 10:47 AM
 */
public class Logger {

    private static final int STACK_TRACE_LEVELS_UP = 5;
    private static boolean LOGGING_ENABLED = true;

    public static void error(String tag, String message) {
        if (LOGGING_ENABLED) {
            Log.e(tag, getClassNameMethodNameAndLineNumber() + message);

        }
    }

    public static void debug(String tag, String message) {
        if (LOGGING_ENABLED) {
            Log.d(tag, getClassNameMethodNameAndLineNumber() + message);

        }
    }

    public static void info(String tag, String message) {
        if (LOGGING_ENABLED) {
            Log.i(tag, getClassNameMethodNameAndLineNumber() + message);

        }
    }

    /**
     * Get the current line number. Note, this will only work as called from
     * this class as it has to go a predetermined number of steps up the stack
     * trace. In this case 5.
     *
     * @return int - Current line number.
     * @author kvarela
     */
    private static int getLineNumber() {
        return Thread.currentThread().getStackTrace()[STACK_TRACE_LEVELS_UP].getLineNumber();
    }

    /**
     * Get the current class name. Note, this will only work as called from this
     * class as it has to go a predetermined number of steps up the stack trace.
     * In this case 5.
     *
     * @return String - Current line number.
     * @author kvarela
     */
    private static String getClassName() {
        String fileName = Thread.currentThread().getStackTrace()[STACK_TRACE_LEVELS_UP].getFileName();

        // kvarela: Removing ".java" and returning class name
        return fileName.substring(0, fileName.length() - 5);
    }

    /**
     * Get the current method name. Note, this will only work as called from
     * this class as it has to go a predetermined number of steps up the stack
     * trace. In this case 5.
     *
     * @return String - Current line number.
     * @author kvarela
     */
    private static String getMethodName() {
        return Thread.currentThread().getStackTrace()[STACK_TRACE_LEVELS_UP].getMethodName();
    }

    /**
     * Returns the class name, method name, and line number from the currently
     * executing log call in the form <class_name>.<method_name>()-<line_number>
     *
     * @return String - String representing class name, method name, and line
     * number.
     * @author kvarela
     */
    private static String getClassNameMethodNameAndLineNumber() {
        return "["+getClassName() + "." + getMethodName() + "-->" + getLineNumber() + "]: ";
    }
}
