package com.dusty.commons.util;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.location.Location;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.dusty.commons.helper.MyAlertDialogInterface;
import com.dusty.commons.helper.MyDialogButtonHandle;
import com.dusty.commons.web.dto.AuthToken;
import com.dusty.commons.web.service.util.OauthUtils;
import com.google.gson.Gson;
import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import org.apache.commons.lang.StringUtils;
import org.jsoup.Jsoup;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.List;
import java.util.Locale;

import lombok.Getter;
import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by UYI on 06/07/2016.
 * spiced by chidum
 * tasted by iretioluwa
 */
public class AppUtil extends OauthUtils {

    public static final String TOKEN_TYPE_USER = "token.type.user";
    public static final String TOKEN_TYPE_ADMIN = "token.type.admin";
    public static final String COUNTRIES_KEY = "countries.key";
    private static final String LOGGER_HANDLE = "COMMONS";
    private static final String SERVER_IP_KEY = "hostname";
    private static final String SERVER_PORT_KEY = "port";
    private static final String USER_ACCESS_TOKEN_KEY = "user_access_token";
    private static final String ADMIN_ACCESS_TOKEN_KEY = "admin_access_token";
    //storage permissions
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static final String CURRENT_LOCATION_KEY = "current_location";
    private static boolean debugMode = true;
    private static String[] PERMISSIONS_STORAGE = {Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE};
    private static String[] PERMISSIONS_CAMERA = {Manifest.permission.CAMERA};
    public SharedPrefHelper mPref;
    private Context context;
    private SharedPreferences defaultSharedPreferences;
    @Getter
    private Gson gson;


    public AppUtil(Context context, SharedPrefHelper sharedPrefHelper){
        this.context = context;
        gson = new Gson();
        mPref = sharedPrefHelper;

    }

    public AppUtil(Activity activity, SharedPrefHelper sharedPrefHelper){
        this.context = activity;
        gson = new Gson();
        mPref = sharedPrefHelper;
    }

/*    public AppUtil(Activity activity) {
        this.context = activity;
        setUp();
    }

    public AppUtil(Context context) {
        this.context = context;
        setUp();
    }*/

    public static void verifyCameraPermissions(Activity activity) {

        int permission = ActivityCompat.checkSelfPermission(activity,
                Manifest.permission.CAMERA);

        if (permission != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(activity,
                    PERMISSIONS_CAMERA, REQUEST_EXTERNAL_STORAGE);

        }
    }

    public static void verifyStoragePermissions(Activity activity) {
        int permission = ActivityCompat.checkSelfPermission(activity,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {


            ActivityCompat.requestPermissions(activity, PERMISSIONS_STORAGE, REQUEST_EXTERNAL_STORAGE);
        }

    }

    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    /**
     * it cleans up html tags
     *
     * @param html html text to be converted to text
     * @return returns a formatted string
     */


    public static String html2text(String html) {
        return Jsoup.parse(html).text();
    }

    public static String getLogger() {
        return LOGGER_HANDLE;
    }

    public static String capitalizeFirstLetter(String sentence) {

        if (sentence != null && sentence.length() == 1) return sentence.toUpperCase();
        if (sentence != null && sentence.length() > 1)
            return sentence.substring(0, 1).toUpperCase() + sentence.substring(1);

        return sentence;
    }

  public enum Level{

        INFO, DEBUG, ERROR
  }
    public static void debug(Level level, String log, String tag, Object...parameters){

        if (log != null && debugMode) {


            String placeHolder = "\\{\\}";

            for(int i=0; i<parameters.length; i++){
                log = log.replaceFirst(placeHolder, String.valueOf(parameters[i]));
            }

            switch (level){
                case INFO:
                    Log.i(tag, log);
                    break;
                case DEBUG:
                    Log.d(tag, log);
                    break;
                case ERROR:
                    Log.e(tag, log);
                    break;

                default:
                    Log.d(tag, log);
                    break;

            }
            
        }

    }

    public static void info(String log, Object...parameters) {

        debug(Level.INFO, log, getLogger(), parameters);

    }

    public static void error(String log, Object...parameters) {

        debug(Level.ERROR, log, getLogger(), parameters);
    }

    public static void log(String log, Object...parameters) {
        debug(Level.DEBUG, log, getLogger(), parameters);
    }

    public static void debug(String log, Object...parameters) {

        debug(Level.DEBUG, log, getLogger(), parameters);
    }

    public static void hideSoftKeyboard(Activity activity) {

        try {
            InputMethodManager inputMethodManager =
                    (InputMethodManager) activity.getSystemService(
                            Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(
                    activity.getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
            error(e.getMessage());
        }

    }

    /**
     * formats double currency amount into String with Naira symbol
     *
     * @param amt amount to be formatted
     * @return the formatted currency
     */
    public static String getFormatedCurrencyAmount(double amt) {

        Locale locale = new Locale("en", "NG");
        NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(locale);
        return currencyFormatter.format(Double.valueOf(amt));
    }

    /**
     * @param formattedAmt formatted currency to be parsed
     * @return parsed double data type of the amount in String data type
     * @throws ParseException invalid number format will be thrown
     */
    public static Number parseCurrencyToNumber(String formattedAmt) throws ParseException {

        Locale locale = new Locale("en", "NG");
        NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(locale);

        return currencyFormatter.parse(formattedAmt);
    }

    /**
     * This method hides the status and navigation bar in an activity
     * <p>
     * to make content appear behind status bar use: To do this, use SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN.
     * You may also need to use SYSTEM_UI_FLAG_LAYOUT_STABLE to help your app maintain a stable layout
     * <p>
     * Also set android:fitsSystemWindows to true in the parent layout of the activity to allow the window to
     * adjust its content accordingly
     * note: on touch the navigation and status bar will appear and should be called before
     * calling the setContentView method
     *
     * @param decorView                the view of that activity. decoView= getWindow().getDecorView()
     * @param placeContentBehindStatus true if you also want the activity to behind the status bar
     */
    public static void hideStatusAndNavigationBar(View decorView, boolean placeContentBehindStatus) {
        int uiOptions;
        if (!placeContentBehindStatus) {

            uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN;
        } else {

            uiOptions = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE;
        }

        decorView.setSystemUiVisibility(uiOptions);
    }

    public static String resizeAndCompressImageBeforeSend(Context context, String filePath, String fileName, int maxFileSize) {
        // final int MAX_IMAGE_SIZE = 700 * 1024; // max final file size in kilobytes
        final int MAX_IMAGE_SIZE = maxFileSize; // max final file size in kilobytes


        // First decode with inJustDecodeBounds=true to check dimensions of image
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filePath, options);

        // Calculate inSampleSize(First we are going to resize the image to 800x800 image, in order to not have a big but very low quality image.
        //resizing the image will already reduce the file size, but after resizing we will check the file size and start to compress image
        options.inSampleSize = calculateInSampleSize(options, 800, 800);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;

        Bitmap bmpPic = BitmapFactory.decodeFile(filePath, options);


        int compressQuality = 100; // quality decreasing by 5 every loop.
        int streamLength;
        do {
            ByteArrayOutputStream bmpStream = new ByteArrayOutputStream();
            Log.d("compressBitmap", "Quality: " + compressQuality);
            bmpPic.compress(Bitmap.CompressFormat.JPEG, compressQuality, bmpStream);
            byte[] bmpPicByteArray = bmpStream.toByteArray();
            streamLength = bmpPicByteArray.length;
            compressQuality -= 5;
            Log.d("compressBitmap", "Size: " + streamLength / 1024 + " kb");
        } while (streamLength >= MAX_IMAGE_SIZE);

        try {
            //save the resized and compressed file to disk cache
            Log.d("compressBitmap", "cacheDir: " + context.getCacheDir());
            FileOutputStream bmpFile = new FileOutputStream(context.getCacheDir() + fileName);
            bmpPic.compress(Bitmap.CompressFormat.JPEG, compressQuality, bmpFile);
            bmpFile.flush();
            bmpFile.close();
        } catch (Exception e) {
            Log.e("compressBitmap", "Error on saving file");
        }
        //return the path of resized and compressed file
        return context.getCacheDir() + fileName;
    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        String debugTag = "MemoryInformation";
        // Image nin islenmeden onceki genislik ve yuksekligi
        final int height = options.outHeight;
        final int width = options.outWidth;
        Log.d(debugTag, "image height: " + height + "---image width: " + width);
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }
        Log.d(debugTag, "inSampleSize: " + inSampleSize);
        return inSampleSize;
    }

    public void setUp() {
        //creates shared preference helper

        defaultSharedPreferences =
                PreferenceManager.getDefaultSharedPreferences(context);

        /*defaultSharedPreferences = context.getSharedPreferences("app_commons",
                Context.MODE_PRIVATE);*/

        mPref = new DefaultSharedPrefHelper(context.getResources(), defaultSharedPreferences);
        gson = new Gson();
    }

    public String formatAmount(BigDecimal bigDecimal) {
        DecimalFormat df = new DecimalFormat("#,###.00");

        String formattedString = df.format(bigDecimal);
        if (formattedString.equals(".00")) return "0.00";
        return formattedString;
    }

    public void toastLong(String message) {
        Toast.makeText(this.context, message, Toast.LENGTH_LONG).show();
    }


    public String getAdminTokenBaseUrl() {
      //  return mPref.getString(mPref.getString(R.string.admin_token_url), "");
        return mPref.getString("admin_token_url", "");
        //return "http://41.78.157.142:9999";
    }

    /**
     * @deprecated Use TokenServiceParams tokenBaseURL to set your token base url
     * @param adminTokenBaseUrl url for the oauth, has been Deprecated
     */
    @Deprecated
    public void setAdminTokenBaseUrl(String adminTokenBaseUrl) {

      //  mPref.applyString(mPref.getString(R.string.admin_token_url), adminTokenBaseUrl);
        mPref.applyString("admin_token_url", adminTokenBaseUrl);
    }

    /**
     * gets the base api url
     *
     * @return returns the base url
     */
    public String getApiBaseUrl() {

        return "http://41.78.157.142:9443";
    }


    public Location getSavedLocation() {

        String formallyStoredLocation = mPref
                .getString(CURRENT_LOCATION_KEY, "");

        if (formallyStoredLocation.isEmpty()) return null;

        try {

            Location current =
                    gson.fromJson(formallyStoredLocation, Location.class);

            return current;

        } catch (Exception e) {

        }

        return null;
    }

    public void resetStoredLocation() {
        mPref.applyString(CURRENT_LOCATION_KEY, "");
    }

    public boolean validateAndSaveLocation(Location location) {

        if (location.getAccuracy() < 30) {

            Gson gson = new Gson();

            String stringLocation = gson.toJson(location);
            AppUtil.debug("location accuracy is less than 30, acceptable will save");


            //check to see if there is any other location with a better accuracy already stored
            boolean updateSavedLocation = false;

            String formallyStoredLocation = mPref.getString(CURRENT_LOCATION_KEY, "");

            if (formallyStoredLocation.isEmpty()) {
                updateSavedLocation = true;
            } else {

                try {

                    Location current =
                            gson.fromJson(formallyStoredLocation, Location.class);

                    updateSavedLocation = true;

                } catch (Exception e) {

                    AppUtil.error(e.getMessage());

                }

            }

            if (updateSavedLocation) {
                AppUtil.debug("saving new location");
                mPref.applyString(CURRENT_LOCATION_KEY, stringLocation);

                AppUtil.debug("returning location saved");

                return true;
            }

        }


        return false;
    }

    private static final String USER_DATA_UPDATED = "userdata_updated";
    private static final String BASE_DATA_FLAG = "base_data_flag";
    private static final String LOGIN_STATUS = "login_status";

    public void setUserDataProfileDownloadedFlag(boolean flag) {
        mPref.applyBoolean(USER_DATA_UPDATED, flag);
    }

    public boolean hasBaseDataBeenDownloaded() {
        //hardcoded to force download of data manual
        return mPref.getBoolean(USER_DATA_UPDATED, false);
    }

    public void setBaseDataDownloadedFlag() {

        debug("about setting basedata to downloaded");

        mPref.applyBoolean(BASE_DATA_FLAG, true);
    }

    public void setLoginStatus(boolean status) {
        mPref.applyBoolean(LOGIN_STATUS, status);
    }

    private static String USERNAME_KEY = "username";
    private static String PASSWORD_KEY = "password";

    public void saveSuccessfulLoginCredentials(String username, String password) {
       // mPref.applyString(R.string.username, username);
        //mPref.applyString(R.string.password, password);
        log("storing the login credentials");
        mPref.applyString(PASSWORD_KEY, password);
        mPref.applyString(USERNAME_KEY, username);

        setLoginStatus(true);
    }

    public String getLoggedInUsername() {
        //this is for porting old versions to the new version
        return mPref.getString(USERNAME_KEY, "");
    }

    public String getLoggedInPassword() {
        return mPref.getString(PASSWORD_KEY, "");
    }

    @Override
    public boolean isLoggedIn() {
        return getLoggedInStatus();
    }

    public boolean getLoggedInStatus() {
        return mPref.getBoolean(LOGIN_STATUS, false);
    }

    public DecimalFormat getDecimalFormatter() {
        return new DecimalFormat("#,###.00");
    }

    public String convertToApiNumberFormat(BigDecimal amount) {

        //multiply by 100
        amount = amount.multiply(new BigDecimal(100));
        amount = amount.setScale(0, RoundingMode.UP);

        NumberFormat format = DecimalFormat.getInstance();
        format.setRoundingMode(RoundingMode.FLOOR);
        format.setMinimumFractionDigits(0);
        format.setMaximumFractionDigits(2);
        //this is crude change this later
        return format.format(amount).replaceAll(",", "");
    }

    public float dpToPixel(int dp) {

        Resources r = context.getResources();
        float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics());

        return px;
    }

    public int getPhoneWidth() {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);


        int width = size.x;
        int height = size.y;

        return width;
    }

    public int getPhoneHeight() {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);


        int width = size.x;
        int height = size.y;

        return height;
    }

    /**
     * gets the stored token from the shared prefs of the application
     * depending on the type of token either user or admin
     *
     * @param tokenType type of token e.g
     * @return auth token
     */
    public AuthToken getToken(String tokenType) {

        AppUtil.debug("about to get token for token type " + tokenType + " from the saved params");
        String storedToken = "";

        if (tokenType != null) {

            if (TOKEN_TYPE_ADMIN.equalsIgnoreCase(tokenType)) {

                AppUtil.debug("looking for the admin token");
                storedToken = mPref.getString(ADMIN_ACCESS_TOKEN_KEY, "");

            } else if (TOKEN_TYPE_USER.equalsIgnoreCase(tokenType)) {

                AppUtil.debug("looking for the user token");
                storedToken = mPref.getString(USER_ACCESS_TOKEN_KEY, "");
            }
        }

        if (storedToken.equalsIgnoreCase("")) {
            return null;
        } else {
            try {
                return new Gson().fromJson(storedToken, AuthToken.class);

            } catch (Exception e) {
                return null;
            }
        }
    }

    /**
     * saves the token in the shared pref of the application
     *
     * @param tokenType token type
     * @param token token to be saved
     */
    public void saveToken(String tokenType, AuthToken token) {

        if (tokenType != null) {

            if (TOKEN_TYPE_ADMIN.equalsIgnoreCase(tokenType)) {

                AppUtil.debug("saving token type admin");
                mPref.applyString(ADMIN_ACCESS_TOKEN_KEY, new Gson().toJson(token));

            } else if (TOKEN_TYPE_USER.equalsIgnoreCase(tokenType)) {

                AppUtil.debug("saving token type user");
                mPref.applyString(USER_ACCESS_TOKEN_KEY, new Gson().toJson(token));
            }
        }

    }

    /**
     * return a picasso instance with oauth header already included
     *
     * @return picasso instance
     */
    public Picasso getPicasso() {

        File cacheDir = new File(context.getCacheDir(), "imageCache");
        Cache myCache = new Cache(cacheDir, Integer.MAX_VALUE);

        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request newRequest = chain.request().newBuilder()
                                .addHeader("Authorization", "Bearer " + getToken(TOKEN_TYPE_USER).getAccessToken())
                                .build();
                        return chain.proceed(newRequest);
                    }
                })
                .cache(myCache)
                .build();


        Picasso picasso = new Picasso.Builder(context)
                .downloader(new OkHttp3Downloader(client))
                // .indicatorsEnabled(true)
                .build();

        return picasso;
    }

    public void toastShort(String display) {
        Toast.makeText(context, display, Toast.LENGTH_LONG).show();
    }

    public void smsShare(String message) {
        Intent smsIntent = new Intent(Intent.ACTION_VIEW);
        smsIntent.setType("vnd.android-dir/mms-sms");
//          smsIntent.putExtra("address", "12125551212");
        smsIntent.putExtra("sms_body", message);
        context.startActivity(smsIntent);

    }

    public void emailShare(String message) {

        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", "", null));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Meter Token");
        emailIntent.putExtra(Intent.EXTRA_TEXT, message);
        context.startActivity(Intent.createChooser(emailIntent, "Sending Email..."));

    }

    public void copyToClipboard(String message) {

        ClipboardManager clipBoard = (ClipboardManager) context.getSystemService(context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("token_amount_date", message);
        if (clipBoard != null) {
            clipBoard.setPrimaryClip(clip);
        }
        toastShort("Copied to Clipboard");

    }

    /**
     * An over loaded method that creates a simple spinner with defaults layouts for the views
     *
     * @param spinner      the spinner object to be initialized
     * @param dropDownList a list of the string to be initialized into spinner list
     * @param listener     the listener for the click action of each item on the list
     */
    public void setupSpinner(Spinner spinner, List<String> dropDownList,
                             @Nullable AdapterView.OnItemSelectedListener listener) {

        setupSpinner(spinner, dropDownList, android.R.layout.simple_spinner_item,
                android.R.layout.simple_spinner_dropdown_item, listener);

    }

    /**
     * helper method for creating a simple spinner with layouts you specify for the views
     *
     * @param spinner         the spinner object to be initialized
     * @param dropdownItem    the layout to use when the list of choices appears
     * @param spinnerTextItem the layout of the item of the spinner when its closed
     * @param dropDownList    a list of the string to be initialized into spinner list
     * @param listener        the listener for the click action of each item on the list
     */
    public void setupSpinner(Spinner spinner, List<String> dropDownList, @LayoutRes int spinnerTextItem, @LayoutRes int dropdownItem,
                             @Nullable AdapterView.OnItemSelectedListener listener) {

        ArrayAdapter<String> spinnerAdapterAdapter;

        //set Spinner's listener
        if (listener != null) {
            spinner.setOnItemSelectedListener(listener);
        }

        spinnerAdapterAdapter = new ArrayAdapter<>(context, spinnerTextItem, dropDownList);
        spinnerAdapterAdapter.setDropDownViewResource(dropdownItem);

        spinner.setAdapter(spinnerAdapterAdapter);
    }

    /**
     *
     * @param title title of the message
     * @param message message
     * @param isCancelable should enable cancel button
     * @param buttonHandle button ref
     * @param alertDialogInterface implementation
     */
    public void showCustomAlertDialog(@Nullable String title, @NonNull String message, @NonNull boolean isCancelable
            , @Nullable MyDialogButtonHandle buttonHandle, @Nullable final MyAlertDialogInterface alertDialogInterface) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        if (title != null) {
            builder.setTitle(title);
        }

        builder.setMessage(message);
        builder.setCancelable(isCancelable);

        if (buttonHandle != null) {
            if (buttonHandle.getPositiveButtonHandle() != null) {
                builder.setPositiveButton(buttonHandle.getPositiveButtonHandle(), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        assert alertDialogInterface != null;
                        alertDialogInterface.onPositiveButtonClicked(dialog);
                    }
                });
            }
            if (buttonHandle.getNeutralButtonHandle() != null) {

                builder.setNegativeButton(buttonHandle.getNeutralButtonHandle(), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        assert alertDialogInterface != null;
                        alertDialogInterface.onNeutralButtonClicked(dialog);

                    }
                });
            }
            if (buttonHandle.getNegativeButtonHandle() != null) {

                builder.setNeutralButton(buttonHandle.getNegativeButtonHandle(), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        assert alertDialogInterface != null;
                        alertDialogInterface.onNegativeButtonClicked(dialog);

                    }
                });
            }
        }

        AlertDialog dialog = builder.create();
        dialog.show();

    }
}