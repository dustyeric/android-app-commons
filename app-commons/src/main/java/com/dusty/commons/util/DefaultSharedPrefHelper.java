package com.dusty.commons.util;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.content.res.Resources;

/**
 * @author .: Chukwudum Ekwueme
 * email ..: chidumekwueme@gmail.com, chukwudum.ekwueme@cwlgroup.com
 * created : Feb 29, 2016 1:21 PM
 */
@SuppressLint("CommitPrefEdits")
public class DefaultSharedPrefHelper extends SharedPrefHelper {

    public DefaultSharedPrefHelper(Resources res, SharedPreferences preferences) {
        mRes = res;
        mPref = preferences;
    }

}