package com.dusty.commons.helper;

import android.content.DialogInterface;

/**
 * @author .: Oriaghan Uyi
 * email ..: oriaghan3@gmail.com, uyi.oriaghan@cwg-plc.com
 * created : 8/30/17 6:20 PM
 */
abstract public class MyAlertDialogInterface {

    public MyAlertDialogInterface() {

    }

    public void onPositiveButtonClicked(DialogInterface dialog){

    }

    public void onNeutralButtonClicked(DialogInterface dialog) {

    }

    public void onNegativeButtonClicked(DialogInterface dialog) {
        dialog.dismiss();
    }

}
