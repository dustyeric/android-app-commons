package com.dusty.commons.helper;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.dusty.commons.R;


/**
 * @author .: Chukwudum Ekwueme
 * email ..: chidumekwueme@gmail.com, chukwudum.ekwueme@cwlgroup.com
 * created : 12/12/16
 */
public class ToolBarHelper {

    private AppCompatActivity context;

    private Toolbar toolbar;

    private TextView title;

    private View backIcon;

    public ToolBarHelper(AppCompatActivity context, Toolbar toolbar,
                         final OnBackPressedListener onBackPressedListener, int backIconResource) {

        this.context = context;
        this.toolbar = toolbar;

        title = toolbar.findViewById(R.id.title);


        backIcon = toolbar.findViewById(backIconResource);


        if (onBackPressedListener != null) {
            backIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onBackPressedListener.onClick();
                }
            });

        } else {

            backIcon.setVisibility(View.GONE);
        }

    }

    public static ToolBarHelper build(AppCompatActivity context,
                                      Toolbar toolbar,
                                      final OnBackPressedListener onBackPressedListener,
                                      int backIconRes) {
        return new ToolBarHelper(context, toolbar, onBackPressedListener, backIconRes);
    }

    public static ToolBarHelper build(AppCompatActivity context,
                                      Toolbar toolbar, int backIconRes) {
        return new ToolBarHelper(context, toolbar, null, backIconRes);
    }

    public void disableBackButton() {
        if (backIcon != null) {
            backIcon.setVisibility(View.GONE);
        }
    }

    public void enableBackPress(final OnBackPressedListener onBackPressedListener) {

        if (toolbar != null) {
            backIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressedListener.onClick();
                }
            });

            backIcon.setVisibility(View.VISIBLE);
        }
    }

    public ToolBarHelper title(String title) {

        this.title.setText(title);
        return this;
    }


    public interface OnBackPressedListener {

        void onClick();
    }

}
