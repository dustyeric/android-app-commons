package com.dusty.commons.helper;

import java.io.Serializable;

import lombok.Data;
import lombok.Builder;

/**
 * Created by uyi on 7/6/17.
 */

@Data
@Builder
public class MyDialogButtonHandle implements Serializable {


    private String positiveButtonHandle;
    private String neutralButtonHandle;
    private String negativeButtonHandle;



}
