package com.dusty.commons.web.dto;

import java.io.Serializable;
import java.util.ArrayList;

import lombok.Data;

/**
 * @author : Clement Nosariemen Ojo
 * email *: clement.ojo@live.com, clement.ojo@cwlgroup.com
 * date **: November 16, 2016  14:43 PM
 * -------------------------------------------------------------
 */
@Data
public class ApiErrors implements Serializable {
	private int errorCount;
	private ArrayList<ApiError> apiErrorList;
}
