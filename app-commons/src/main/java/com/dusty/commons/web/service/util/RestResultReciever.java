package com.dusty.commons.web.service.util;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

/**
 * @author .: Chukwudum Ekwueme
 * email ..: chidumekwueme@gmail.com, chukwudum.ekwueme@cwlgroup.com
 * created : 6/16/16
 */
public class RestResultReciever extends ResultReceiver {

    private Receiver mReceiver;

    public RestResultReciever(Handler handler) {
        super(handler);
    }

    public void setReceiver(Receiver receiver) {
        mReceiver = receiver;
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {

        if (mReceiver != null) {
            mReceiver.onReceiveResult(resultCode, resultData);
        }
    }

    public interface Receiver {
        void onReceiveResult(int resultCode, Bundle resultData);

    }
}