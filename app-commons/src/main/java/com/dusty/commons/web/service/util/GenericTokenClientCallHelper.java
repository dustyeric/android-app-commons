package com.dusty.commons.web.service.util;

import android.os.Bundle;
import android.os.ResultReceiver;

import com.dusty.commons.web.dto.ApiResponse;
import com.dusty.commons.web.dto.CommonsResponseWrapper;
import com.dusty.commons.web.dto.CommonsResponseStatus;
import com.dusty.commons.web.service.TokenService;

import retrofit2.Call;

/**
 * @author .: Chukwudum Ekwueme
 * email ..: chidumekwueme@gmail.com, chukwudum.ekwueme@cwlgroup.com
 * created : 12/10/16
 */
public abstract class GenericTokenClientCallHelper<R, S> {

    private final Class<S> typeParameterClass;
    private TokenService tokenService;
    private ResultReceiver resultReceiver;
    private String baseUrl;
    private String operation;
    private String tokenType;

    protected GenericTokenClientCallHelper(TokenService tokenService,
                                           String baseUrl,
                                           String tokenType,
                                           Class<S> typeParameterClass) {

        this.tokenService = tokenService;
        this.resultReceiver = tokenService.getRecieverHandle();
        this.baseUrl = baseUrl;
        this.operation = tokenService.getIntent().getAction();
        this.tokenType = tokenType;
        this.typeParameterClass = typeParameterClass;
    }

    public abstract Call<R> createServiceCall(S client);


    private Call<R> makeServiceCall(S client){
        return createServiceCall(client);
    }


    public R makeCall(boolean needResponse) {
        return new CommonsClientHelper<S,
                R>(baseUrl,
                typeParameterClass,
                tokenType,
                tokenService){
            public Call<R> createServiceCall(S client){
                return makeServiceCall(client);
            }
        }.makeApiCall().getResponse();
    }



    public void makeCall() {

        CommonsResponseWrapper<R> responseWrapper = new CommonsClientHelper<S,
                R>(baseUrl,
                typeParameterClass,
                tokenType,
                tokenService){
            public Call<R> createServiceCall(S client){
                return makeServiceCall(client);
            }
        }.makeApiCall();

        Bundle bundle = new Bundle();
        bundle.putString(ServiceConstants.OPERATION, operation);
        bundle.putInt(ServiceConstants.HTTP_ERROR_CODE, responseWrapper.getHttpCode());
        if(responseWrapper.getRxResponseStatus().equals(CommonsResponseStatus.FAILED)){
            bundle.putSerializable(ServiceConstants.API_ERROR, responseWrapper.getStatusMessage());
            if(responseWrapper.getResponse() != null){
                bundle.putSerializable(ServiceConstants.SERVICE_RESPONSE_TAG, (ApiResponse)responseWrapper.getResponse());
            }
        }
        if(responseWrapper.getResponse() != null){
            bundle.putSerializable(ServiceConstants.SERVICE_RESPONSE_TAG, (ApiResponse)responseWrapper.getResponse());
        }

        resultReceiver.send(responseWrapper.toLegacyStatus(), bundle);
    }
}