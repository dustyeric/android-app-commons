package com.dusty.commons.web.service.util;

/**
 * @author .: Chukwudum Ekwueme
 * email ..: chidumekwueme@gmail.com, chukwudum.ekwueme@cwlgroup.com
 * created : 5/20/17 9:34 AM
 */
public interface ServiceConstants {

    public static String RECEIVER_TAG = "receiverTag";
    public static String SERVICE_RESPONSE_TAG = "serviceResponse";
    public static String OPERATION = "operation";
    public static String FAILURE_MESSAGE = "failure.message";
    public static String API_ERROR = "api.error";
    public static String PAGE_NUMBER = "page.number";
    public static String HTTP_ERROR_CODE = "http.error.code";

    String SERVER_UNAVAILABLE = "Server is unavailable";

    public static int SUCCESS = 0;
    public static int FAILED = 99;
    public static int INVALID_USER_PASSWORD_COMBO = 99999;
    public static int GENERAL_API_UNAVAILABLE = 992;


    public static int HTTP_UNAUTHORIZED = 401;
    public static int HTTP_CONFLICT = 409;
    public static int HTTP_NOT_FOUND = 404;



}
