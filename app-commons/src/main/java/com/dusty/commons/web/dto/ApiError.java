package com.dusty.commons.web.dto;

import java.io.Serializable;

import lombok.Data;

/**
 * @author : Clement Nosariemen Ojo
 * email *: clement.ojo@live.com, clement.ojo@cwlgroup.com
 * date **: November 16, 2016  14:43 PM
 * -------------------------------------------------------------
 */
@Data
public class ApiError implements Serializable {

	private String userMessage;
	private String developerMessage;
	private String errorCode;
	private String moreInfo;


}