package com.dusty.commons.web.service.util;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.ResultReceiver;
import android.preference.PreferenceManager;

import com.dusty.commons.util.AppUtil;
import com.dusty.commons.util.DefaultSharedPrefHelper;
import com.dusty.commons.util.SharedPrefHelper;

import lombok.Getter;

/**
 * @author .: Chukwudum Ekwueme
 * email ..: chidumekwueme@gmail.com, chukwudum.ekwueme@cwlgroup.com
 * created : 7/14/16
 */
public class ServiceBase {

    protected Context context;
    @Getter
    public OauthUtils oauthUtils;

    @Getter
    public Intent intent;

    public ServiceBase(OauthUtils oauthUtils){
        this.oauthUtils = oauthUtils;
    }

    public ServiceBase(Context context, Intent intent){
        this.context = context;

        SharedPreferences defaultSharedPreferences =
                PreferenceManager.getDefaultSharedPreferences(context);

        oauthUtils = new AppUtil(context, new DefaultSharedPrefHelper(context.getResources(),
                defaultSharedPreferences));
        this.intent = intent;
    }

    public ServiceBase(Context context, Intent intent, OauthUtils oauthUtils){
        this.context = context;
        this.oauthUtils = oauthUtils;
        this.intent = intent;
    }

    protected ResultReceiver getRecieverHandle(){

        return intent.getParcelableExtra(ServiceConstants.RECEIVER_TAG);
    }
}
