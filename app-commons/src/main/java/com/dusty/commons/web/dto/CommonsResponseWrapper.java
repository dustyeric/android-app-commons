package com.dusty.commons.web.dto;


import com.dusty.commons.web.service.util.ServiceConstants;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

/**
 * @author .: Chukwudum Ekwueme
 * email ..: chidumekwueme@gmail.com, chukwudum.ekwueme@cwlgroup.com
 * created : Oct 01, 2018 8:06 AM
 */

@Data
@AllArgsConstructor
public abstract class CommonsResponseWrapper<T> {


    private T response;

    private CommonsResponseStatus rxResponseStatus;

    private String statusMessage;

    private int httpCode;

    private String rawErrorResponse;


    public int toLegacyStatus(){
        switch (rxResponseStatus){
            case FAILED:
                return ServiceConstants.FAILED;
            case SUCCESS:
                return ServiceConstants.SUCCESS;
            case GENERAL_API_UNAVAILABLE:
                return ServiceConstants.GENERAL_API_UNAVAILABLE;
            case NETWORK_ERROR:
                return ServiceConstants.GENERAL_API_UNAVAILABLE;
                default:
                    return ServiceConstants.GENERAL_API_UNAVAILABLE;
        }
    }

}
