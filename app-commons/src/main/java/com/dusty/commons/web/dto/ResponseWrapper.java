package com.dusty.commons.web.dto;

import java.io.Serializable;

import lombok.Data;
import lombok.Builder;

/**
 * @author .: Chukwudum Ekwueme
 * email ..: chidumekwueme@gmail.com, chukwudum.ekwueme@cwlgroup.com
 * created : Jan 11, 2018 11:08 AM
 */

@Data
@Builder
public class ResponseWrapper implements Serializable {

    private Object response;

}
