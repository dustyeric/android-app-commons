package com.dusty.commons.web.dto;

import java.io.Serializable;

import lombok.Data;
import lombok.Builder;

/**
 * @author .: Chukwudum Ekwueme
 * email ..: chidumekwueme@gmail.com, chukwudum.ekwueme@cwlgroup.com
 * created : 12/15/16
 */
@Data
@Builder
public class GenerateLoginTokenResponse implements Serializable {

    private AuthToken token;

    private boolean wasSuccessFul;

    private String error;
}
