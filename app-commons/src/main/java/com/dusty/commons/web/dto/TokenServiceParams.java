package com.dusty.commons.web.dto;

import java.io.Serializable;

import lombok.Data;
import lombok.Builder;

/**
 * @author .: Chukwudum Ekwueme
 * email ..: chidumekwueme@gmail.com, chukwudum.ekwueme@cwlgroup.com
 * created : 5/20/17 9:49 AM
 */
@Builder
@Data
public class TokenServiceParams implements Serializable{

    private String adminClientID;

    private String adminClientSecret;

    private String adminScope;

    private String adminUserName;

    private String adminPassword;

    private String adminGrantType;

    private String userTokenGrantType;

    private String userTokenClientId;

    private String userTokenScope;

    private String userTokenClientSecret;

    private String tokenBaseURL;
}
