package com.dusty.commons.web.service.util;

import android.os.Bundle;
import android.os.ResultReceiver;

import com.dusty.commons.web.dto.ApiResponse;
import com.dusty.commons.web.dto.CommonsResponseWrapper;
import com.dusty.commons.web.dto.ResponseWrapper;
import com.dusty.commons.web.dto.CommonsResponseStatus;

import retrofit2.Call;
import retrofit2.Response;

/**
 * @author .: Chukwudum Ekwueme
 * email ..: chidumekwueme@gmail.com, chukwudum.ekwueme@cwlgroup.com
 * created : 12/15/16
 */


public abstract class GenericClientCallHelper<R, S> {

    final Class<S> typeParameterClass;
    private ResultReceiver resultReceiver;
    private String baseUrl;
    private String operation;
    private boolean debugMode;
    private int failureCallNumber;
    private int callNumber;

    public GenericClientCallHelper(ResultReceiver resultReceiver, String baseUrl, String operation,
                                   Class<S> typeParameterClass, boolean debugMode) {
        this.resultReceiver = resultReceiver;
        this.baseUrl = baseUrl;
        this.operation = operation;
        this.typeParameterClass = typeParameterClass;
        this.callNumber = 1;
        this.debugMode = debugMode;
    }

    public abstract Call<R> createServiceCall(S client);

    public S createService() {

        return ServiceGenerator
                .createService(typeParameterClass, baseUrl, debugMode);

    }

    public void handleFailedResponse(Response<R> response, Bundle bundle) {

        bundle.putSerializable(ServiceConstants.SERVICE_RESPONSE_TAG,
                ResponseWrapper.builder().response(response.errorBody()).build());
    }


    /**
     *
     *  handles multiple responses and wraps them in an object
     * @param response response object instance
     * @param bundle android bundle
     */
    public void handleSuccessResponse(R response, Bundle bundle) {

        bundle.putSerializable(ServiceConstants.SERVICE_RESPONSE_TAG,
                ResponseWrapper.builder().response(response).build());

    }

    public void handleSuccessResponse(ApiResponse apiResponse, Bundle bundle, boolean generic) {

        //default at the response to the bundle
        bundle.putString(ServiceConstants.OPERATION, operation);
        bundle.putSerializable(ServiceConstants.SERVICE_RESPONSE_TAG, apiResponse);

    }

    private Call<R> makeServiceCall(S client){
        return createServiceCall(client);
    }

    /**
     * makes a direct call and returns a response specified
     *
     * @return returns response object
     */
    public R makeDirectCall() {
        return new CommonsClientHelper<S,
                        R>(baseUrl,
                typeParameterClass,
                debugMode){
            public Call<R> createServiceCall(S client){
                return makeServiceCall(client);
            }
        }.makeApiCall().getResponse();
    }

    /**
     * makes a call and returns the response though the normal bundle
     */
    public void makeCall() {

        CommonsResponseWrapper<R> responseWrapper = new CommonsClientHelper<S,
                R>(baseUrl,
                typeParameterClass,
                debugMode){
            public Call<R> createServiceCall(S client){
                return makeServiceCall(client);
            }
        }.makeApiCall();

        Bundle bundle = new Bundle();
        bundle.putString(ServiceConstants.OPERATION, operation);
        bundle.putInt(ServiceConstants.HTTP_ERROR_CODE, responseWrapper.getHttpCode());
        if(responseWrapper.getRxResponseStatus().equals(CommonsResponseStatus.FAILED)){
            bundle.putSerializable(ServiceConstants.API_ERROR, responseWrapper.getStatusMessage());
        }
        if(responseWrapper.getResponse() != null){
            bundle.putSerializable(ServiceConstants.SERVICE_RESPONSE_TAG, (ApiResponse)responseWrapper.getResponse());
        }

        resultReceiver.send(responseWrapper.toLegacyStatus(), bundle);

    }
}