package com.dusty.commons.web.dto;

/**
 * @author .: Chukwudum Ekwueme
 * email ..: chidumekwueme@gmail.com, chukwudum.ekwueme@cwlgroup.com
 * created : Oct 01, 2018 9:27 AM
 */
public enum ApiSecurity {
    OAUTH,BASIC,NONE
}
