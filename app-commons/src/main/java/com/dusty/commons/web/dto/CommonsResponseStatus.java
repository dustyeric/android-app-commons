package com.dusty.commons.web.dto;

/**
 * @author .: Chukwudum Ekwueme
 * email ..: chidumekwueme@gmail.com, chukwudum.ekwueme@cwlgroup.com
 * created : Oct 01, 2018 8:09 AM
 */
public enum CommonsResponseStatus {
    SUCCESS,FAILED,NETWORK_ERROR,GENERAL_API_UNAVAILABLE
}
