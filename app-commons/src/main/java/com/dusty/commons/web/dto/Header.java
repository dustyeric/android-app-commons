package com.dusty.commons.web.dto;

import java.io.Serializable;

import lombok.Data;

/**
 * @author .: Chukwudum Ekwueme
 * email ..: chidumekwueme@gmail.com, chukwudum.ekwueme@cwlgroup.com
 * created : 5/27/17 8:36 PM
 */

@Data
public class Header implements Serializable {

    private String headerType;

    private Object value;
}
