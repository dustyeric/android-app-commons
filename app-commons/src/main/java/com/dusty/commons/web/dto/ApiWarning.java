package com.dusty.commons.web.dto;

import lombok.Data;

/**
 * @author : Clement Nosariemen Ojo
 * email *: clement.ojo@live.com, clement.ojo@cwlgroup.com
 * date **: November 16, 2016  14:43 PM
 * -------------------------------------------------------------
 */
@Data
public class ApiWarning {

	/**
	 * "userMessage": "Sorry, the requested resource does not exist",
	 * 05
	 * "developerMessage": "No car found in the database",
	 * 06
	 * "code": 34,
	 * 07
	 * "more info": "http://dev.mwaysolutions.com/blog/api/v1/errors/12345"
	 */
	private String userMessage ;
	private String internalMessage;
	private String code ;
	private String moreInfo ;

}