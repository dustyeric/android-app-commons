package com.dusty.commons.web.service.util;

import com.dusty.commons.exception.ClientGenerationException;
import com.dusty.commons.util.AppUtil;
import com.dusty.commons.web.dto.ApiResponse;
import com.dusty.commons.web.dto.ApiSecurity;
import com.dusty.commons.web.dto.AuthToken;
import com.dusty.commons.web.dto.CommonsResponseStatus;
import com.dusty.commons.web.dto.CommonsResponseWrapper;
import com.dusty.commons.web.service.TokenService;
import com.google.gson.Gson;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Response;

/**
 * @author .: Chukwudum Ekwueme
 * email ..: chidumekwueme@gmail.com, chukwudum.ekwueme@cwlgroup.com
 * created : Sep 30, 2018 1:39 PM
 */
public abstract class CommonsClientHelper<S, R> {

    private final Class<S> clientClass;
    private int callNumber;
    private String baseURL;
    private TokenService tokenService;
    private static final int HTTP_SERVICE_UNAVAILABLE = 503;
    private boolean isDebugMode;
    private String username;
    private String password;
    private ApiSecurity apiSecurity;

    public abstract Call<R> createServiceCall(S client);
    private static final String SERVER_TIMEOUT_ERROR = "Timeout connecting to server";
    private static final String NO_RESPONSE_FROM_SERVER = "No Response received from server";
    private static final String NO_TRANSACTION_TOKEN_ERROR = "Unable to obtain transaction token";

    public CommonsClientHelper(String baseURL, Class<S> clientClass, boolean isDebugMode) {
        this.clientClass = clientClass;
        this.callNumber = 1;
        this.baseURL = baseURL;
        this.isDebugMode = isDebugMode;
        this.apiSecurity = ApiSecurity.NONE;
    }

    public CommonsClientHelper(String baseURL, Class<S> clientClass, String username, String password, boolean isDebugMode) {
        this.clientClass = clientClass;
        this.callNumber = 1;
        this.baseURL = baseURL;
        this.isDebugMode = isDebugMode;
        this.apiSecurity = ApiSecurity.BASIC;
        this.username = username;
        this.password = password;
    }

    public CommonsClientHelper(String baseURL, Class<S> clientClass, String tokenType, TokenService tokenService) {
        this.clientClass = clientClass;
        this.callNumber = 1;
        this.baseURL = baseURL;
        this.tokenService = tokenService;
        this.tokenService.setTokenType(tokenType);
        this.isDebugMode = tokenService.isDebugMode();
        this.apiSecurity = ApiSecurity.OAUTH;
    }

    private CommonsResponseWrapper<R> handleFailedResponse(Response<R> response) {

        String jsonError = null;

        try {
            if (response.errorBody() != null) {
                AppUtil.error("received code : " + String.valueOf(response.code()));

                //it streams the data and deletes it
                 jsonError = response.errorBody().string();
                AppUtil.error("this is the json here : " + jsonError);

                ApiResponse errorResponse =
                        new Gson().fromJson(jsonError, ApiResponse.class);

                //loads a default error in case nothing returned back from the server
                String error = ServiceConstants.SERVER_UNAVAILABLE;

                AppUtil.error("this is from the errorResponse : " + errorResponse);
                if (errorResponse.getApiErrors().getApiErrorList().size() > 0)
                    error = errorResponse.getApiErrors().getApiErrorList().get(0).getUserMessage();

                return new CommonsResponseWrapper<R>((R)errorResponse, CommonsResponseStatus.FAILED,
                        error,response.code(), jsonError){};

            } else {

                AppUtil.error("no error body to return");
                //return failure to calling method
                return new CommonsResponseWrapper<R>(null, CommonsResponseStatus.FAILED,
                        ServiceConstants.SERVER_UNAVAILABLE,response.code(), null){};
            }

        } catch (Exception e) {

            AppUtil.error("exception caught while trying to ready the error body, not generic api response ,cast to required response");
            AppUtil.error(e.getMessage());
            String error = jsonError == null ? ServiceConstants.SERVER_UNAVAILABLE : "request failed";

            return new CommonsResponseWrapper<R>(null, CommonsResponseStatus.FAILED,
                    error,response.code(), jsonError){};
        }
    }


    private CommonsResponseWrapper<R> handleApiResponse(R responseBody, int responseCode){
        ApiResponse apiResponse = (ApiResponse) responseBody;

        //if it is an api response and the request is not successful
        if (!apiResponse.isRequestSuccessful()) {
            String error = "could not complete command";
            AppUtil.error("this is from the errorResponse : " + apiResponse);
            if (apiResponse.getApiErrors().getApiErrorList().size() > 0)
                error = apiResponse.getApiErrors().getApiErrorList().get(0).getUserMessage();

            return new CommonsResponseWrapper<R>((R)apiResponse, CommonsResponseStatus.FAILED,
                    error, responseCode, null){};
        } else {
            return new CommonsResponseWrapper<R>(responseBody, CommonsResponseStatus.SUCCESS,
                    "", responseCode, null){};
        }
    }


    private S getApiClient() throws ClientGenerationException{
        switch (apiSecurity){
            case NONE:
                return generateSimpleClient();
            case BASIC:
                return generateBASICClient();
            case OAUTH:
                return generateOAUTHClient();
             default:
                 return generateSimpleClient();
        }
    }

    private S generateBASICClient() throws ClientGenerationException{
        try{
            return ServiceGenerator.createService(clientClass, username,
                    password,baseURL, isDebugMode);
        }catch (Exception e){
            throw new ClientGenerationException("Invalid basic credentials");
        }
    }
    private S generateSimpleClient() throws ClientGenerationException{
        try {
            return ServiceGenerator.createService(clientClass, baseURL, isDebugMode);
        }catch (Exception e){
            throw new ClientGenerationException("Unable to generate api client");
        }

    }

    private S generateOAUTHClient() throws ClientGenerationException {
        AppUtil.debug("get initial stored token");
        AuthToken authToken = tokenService.getInitialToken();

        if (authToken == null) {

            AppUtil.debug("failed getting token online so returning server unavailable");
            throw new ClientGenerationException(NO_TRANSACTION_TOKEN_ERROR);
        }

        return ServiceGenerator
                .createService(clientClass, authToken.getAccessToken(),
                        baseURL, isDebugMode);
    }

    private CommonsResponseWrapper<R> retryOAUTHTransactionWithNewToken(Response<R> response){

        AuthToken newToken = tokenService.getToken();

        if (newToken != null) {
            //get a new admin token and start afresh
            AppUtil.debug("got a new user token");
            //recall the api with the new token
            return makeApiCall();

        } else {

            AppUtil.error("e don be, cannot contact server to get token");
            return new CommonsResponseWrapper<R>(null, CommonsResponseStatus.GENERAL_API_UNAVAILABLE,
                    NO_TRANSACTION_TOKEN_ERROR, response.code(), null){};

        }

    }

    public CommonsResponseWrapper<R> makeApiCall() {

        AppUtil.debug("call number : " + callNumber);

        if (callNumber > 3) {
            AppUtil.debug("failed contacting server so returning server unavailable to avoid infinite recursions");
            return new CommonsResponseWrapper<R>(null, CommonsResponseStatus.GENERAL_API_UNAVAILABLE,
                    SERVER_TIMEOUT_ERROR, HTTP_SERVICE_UNAVAILABLE, null){};
        }
        callNumber++;

        try{

            S client = getApiClient();
            Call<R> call = createServiceCall(client);

            try {

                Response<R> response = call.execute();

                if (response.isSuccessful()) {

                    AppUtil.debug("response gotten back successfully");
                    if (response.body() == null) {
                        AppUtil.error("response body was  null Retrying...");
                        return makeApiCall();

                    } else {
                        AppUtil.debug("received response back from the server");
                        R responseBody = response.body();
                        try {
                            if (responseBody instanceof ApiResponse){
                                return handleApiResponse(responseBody, response.code());
                            }else{
                                return new CommonsResponseWrapper<R>(responseBody, CommonsResponseStatus.SUCCESS,
                                        "", response.code(), null){};
                            }

                        } catch (Exception e) {
                            AppUtil.log(e.getMessage());
                            return new CommonsResponseWrapper<R>(null, CommonsResponseStatus.GENERAL_API_UNAVAILABLE,
                                    NO_RESPONSE_FROM_SERVER, HTTP_SERVICE_UNAVAILABLE, null){};
                        }
                    }
                } else {

                    //recursive to call itself if unauthorized as this is an admin function and must go through and is an oauth function
                    if (response.code() == ServiceConstants.HTTP_UNAUTHORIZED && apiSecurity == ApiSecurity.OAUTH) {

                        return retryOAUTHTransactionWithNewToken(response);

                    } else {

                        return handleFailedResponse(response);
                    }
                }

            } catch (IOException ioEx) {
                AppUtil.error(ioEx.getMessage());
                AppUtil.error("error connecting to api, Retrying...");
                return makeApiCall();

            }

        }catch (ClientGenerationException e){
            AppUtil.error("client generation exception {}" , e.getMessage());
            return new CommonsResponseWrapper<R>(null, CommonsResponseStatus.GENERAL_API_UNAVAILABLE,
                    e.getMessage(), HTTP_SERVICE_UNAVAILABLE, null){};
        }


    }


}
