package com.dusty.commons.web.dto;

import java.io.Serializable;

import lombok.Data;

/**
 * @author .: Chukwudum Ekwueme
 * email ..: chidumekwueme@gmail.com, chukwudum.ekwueme@cwlgroup.com
 * created : 12/3/16
 */
@Data
public abstract class ResponseEntity<T> implements Serializable {

    private T body;

    private Headers headers;

}
