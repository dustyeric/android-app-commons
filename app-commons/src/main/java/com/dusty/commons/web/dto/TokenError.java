package com.dusty.commons.web.dto;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import lombok.Data;

/**
 * @author .: Chukwudum Ekwueme
 * email ..: chidumekwueme@gmail.com, chukwudum.ekwueme@cwlgroup.com
 * created : 12/15/16
 */
@Data
public class TokenError implements Serializable {

    private String error;

    @SerializedName("error_description")
    private String errorDescription;
}
