package com.dusty.commons.web.service;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.dusty.commons.util.AppUtil;
import com.dusty.commons.web.dto.AuthToken;
import com.dusty.commons.web.dto.GenerateLoginTokenResponse;
import com.dusty.commons.web.dto.TokenError;
import com.dusty.commons.web.dto.TokenServiceParams;
import com.dusty.commons.web.service.util.OauthUtils;
import com.dusty.commons.web.service.util.ServiceBase;
import com.dusty.commons.web.service.util.ServiceGenerator;
import com.google.gson.Gson;

import lombok.Getter;
import lombok.Setter;
import retrofit2.Call;
import retrofit2.Response;


/**
 * @author .: Chukwudum Ekwueme
 * email ..: chidumekwueme@gmail.com, chukwudum.ekwueme@cwlgroup.com
 * created : 2/6/17
 */
public abstract class TokenService<S> extends ServiceBase {

    public static String OPERATION_GENERATE_LOGIN_TOKEN = "operation.generate.login.token";
    private String error;
    private TokenServiceParams tokenServiceParams;
    private Class<S> typeClass;

    @Setter
    private String tokenType = AppUtil.TOKEN_TYPE_USER;

    @Getter
    private boolean debugMode;

    public TokenService(Class<S> typeClass, OauthUtils oauthUtils, TokenServiceParams tokenServiceParams, boolean debugMode){
        super(oauthUtils);
        this.typeClass = typeClass;
        this.tokenServiceParams = tokenServiceParams;
        this.debugMode = debugMode;
    }

    public TokenService(Context context, Intent intent, TokenServiceParams tokenServiceParams, Class<S> typeClass, boolean debugMode) {
        super(context, intent);
        this.tokenServiceParams = tokenServiceParams;
        this.typeClass = typeClass;
        this.debugMode = debugMode;
    }

    public AuthToken getInitialToken(){
        return obtainRequestToken(this.tokenType);
    }

    public AuthToken getToken(){
        if (tokenType.equals(AppUtil.TOKEN_TYPE_USER)) {
            if(!oauthUtils.isLoggedIn()){
                return null;
            }
            return getUserToken(oauthUtils.getLoggedInUsername(), oauthUtils.getLoggedInPassword());
            //if the token type is admin token
        } else {

           return getAdminToken();
        }
    }

    public AuthToken obtainRequestToken(String tokenType) {

        AppUtil.debug("obtain token called for type  " + tokenType);
        AuthToken authToken = oauthUtils.getToken(tokenType);

        //if it doesn't exist
        if (authToken == null) {

            AppUtil.debug("no token previously saved, now trying online");

            if (tokenType.equalsIgnoreCase(AppUtil.TOKEN_TYPE_ADMIN)) {

                authToken = getAdminToken();

            } else {
                if(!oauthUtils.isLoggedIn()){
                    return null;
                }

                AppUtil.debug("getting user token, logged in name is " + oauthUtils.getLoggedInUsername());
                authToken = getUserToken(oauthUtils.getLoggedInUsername(), oauthUtils.getLoggedInPassword());
            }

            //if token is null here
            //server has problem
            if (authToken == null) {

                Log.d(AppUtil.getLogger(), "failed getting token online so returning server unavailable");
            }

            return authToken;

        } else {
            AppUtil.debug("found a token " + authToken.toString());
            return authToken;
        }

    }

    public GenerateLoginTokenResponse generateLoginToken(String username, String password) {

        AuthToken authToken = getUserToken(username, password);
        if (authToken != null) {

            return GenerateLoginTokenResponse.builder().wasSuccessFul(true).token(authToken).build();
        }

        return GenerateLoginTokenResponse.builder().wasSuccessFul(false).error(error).build();
    }


    public AuthToken getUserToken(String username, String password) {

        AppUtil.log("going online to get user token");

        return getAuthenticationTokenOnline(AppUtil.TOKEN_TYPE_USER,
                username,
                password,
                tokenServiceParams.getUserTokenGrantType(),
                tokenServiceParams.getUserTokenScope(),
                tokenServiceParams.getUserTokenClientId(),
                tokenServiceParams.getUserTokenClientSecret());
    }


    public AuthToken getAdminToken() {
        AppUtil.log("going online to get admin token");
        return getAuthenticationTokenOnline(AppUtil.TOKEN_TYPE_ADMIN,
                tokenServiceParams.getAdminUserName(),
                tokenServiceParams.getAdminPassword(),
                tokenServiceParams.getAdminGrantType(),
                tokenServiceParams.getAdminScope(),
                tokenServiceParams.getAdminClientID(),
                tokenServiceParams.getAdminClientSecret());
    }

    /**
     *
     * @param service retrofit annotated client interface
     * @param username username
     * @param password password
     * @param grantType  grant type
     * @param scope scope for the token
     * @param clientId client  id
     * @param clientSecret client secret
     * @return transaction token oauth
     */
    public abstract Call<AuthToken> obtainTransactionToken(
            S service,
            String username,
            String password,
            String grantType,
            String scope,
            String clientId,
            String clientSecret);

    public AuthToken getAuthenticationTokenOnline(
            String tokenType,
            String username,
            String password,
            String grantType,
            String scope,
            String clientId,
            String clientSecret
    ) {


        AppUtil.log("get token called literally");
        Log.d(AppUtil.getLogger(), tokenServiceParams.getTokenBaseURL());
        Log.d(AppUtil.getLogger(), "about to generate service");

        S service = ServiceGenerator.createService(typeClass,
                clientId, clientSecret, tokenServiceParams.getTokenBaseURL(), debugMode);

        AppUtil.debug("building token call");

        Call<AuthToken> call;

        call = obtainTransactionToken(service, username, password, grantType, scope, clientId, clientSecret);

        AuthToken token = null;

        try {
            Response<AuthToken> response = call.execute();

            if (response.isSuccessful()) {

                if (response.body() != null) {
                    Log.d(AppUtil.getLogger(), "successfully got token");
                    Log.d(AppUtil.getLogger(), "token : " + response.body());
                    token = response.body();
                    Log.d(AppUtil.getLogger(), "saving new token to the system");

                    oauthUtils.saveToken(tokenType, token);

                } else {

                    AppUtil.error("Token is null. don't know what happened");
                    error = "Could not contact server";
                }
            } else {

                AppUtil.error("error getting token");
                AppUtil.error("error code " + response.code());
                String jsonError = response.errorBody().string();

                TokenError tokenError =
                        new Gson().fromJson(jsonError, TokenError.class);

                AppUtil.debug(tokenError.toString());
                error = "Invalid username / password combination";

            }

        } catch (Exception e) {

            AppUtil.error("could not execute request, error : ");

            if (e.getMessage() != null) {

                AppUtil.error(e.getMessage());

            }

            error = "Could not contact server";

        }

        return token;
    }
}
