package com.dusty.commons.web.dto;

import java.io.Serializable;

import lombok.Data;

/**
 * @author .: Chukwudum Ekwueme
 * email ..: chidumekwueme@gmail.com, chukwudum.ekwueme@cwlgroup.com
 * created : 12/12/16
 */
@Data
public class Pageable implements Serializable {

    private int totalElements;
    private int totalPages;
    private boolean last;
   /* private String sort;*/
    private boolean first;
    private int numberOfElements;
    private int size;
    private int number;
}
