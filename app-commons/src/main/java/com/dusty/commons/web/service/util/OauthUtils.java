package com.dusty.commons.web.service.util;

import com.dusty.commons.web.dto.AuthToken;

/**
 * @author .: Chukwudum Ekwueme
 * email ..: chidumekwueme@gmail.com, chukwudum.ekwueme@cwlgroup.com
 * created : Oct 01, 2018 11:09 AM
 */
public abstract class OauthUtils {
    //get the logged in username
    public abstract String getLoggedInUsername();

    //get the logged in password
    public abstract String getLoggedInPassword();

    //implement how a successfully gotten token will be saved
    public abstract void saveToken(String tokenType,AuthToken token);

    //return a saved token depending on the token type
    public abstract AuthToken getToken(String tokenType);

    //check if the user is logged in already
    public abstract boolean isLoggedIn();

    //save successful login credentials
    public abstract void saveSuccessfulLoginCredentials(String username, String password);
}
