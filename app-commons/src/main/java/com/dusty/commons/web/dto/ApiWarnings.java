package com.dusty.commons.web.dto;

import java.util.ArrayList;

import lombok.Data;

/**
 * @author : Clement Nosariemen Ojo
 * email *: clement.ojo@live.com, clement.ojo@cwlgroup.com
 * date **: November 16, 2016  14:43 PM
 * -------------------------------------------------------------
 */
@Data
public class ApiWarnings {
	private int warningCount;
	private ArrayList<ApiWarning> apiWarningList;

}
