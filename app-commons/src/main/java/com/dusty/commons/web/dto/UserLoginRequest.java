package com.dusty.commons.web.dto;

import java.io.Serializable;

import lombok.Data;
import lombok.Builder;

/**
 * @author .: Chukwudum Ekwueme
 * email ..: chidumekwueme@gmail.com, chukwudum.ekwueme@cwlgroup.com
 * created : 12/4/16
 */
@Builder
@Data
public class UserLoginRequest implements Serializable {

    private String username;

    private String password;

}
