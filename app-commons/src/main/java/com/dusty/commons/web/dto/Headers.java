package com.dusty.commons.web.dto;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

/**
 * @author .: Chukwudum Ekwueme
 * email ..: chidumekwueme@gmail.com, chukwudum.ekwueme@cwlgroup.com
 * created : 12/3/16
 */
@Data
public class Headers implements Serializable {

    private int headerCount;

    private List<Header> headerList;

}
