package com.dusty.commons.web.dto;

import java.io.Serializable;

import lombok.Data;

/**
 * @author : Clement Nosariemen Ojo
 * email *: clement.ojo@live.com, clement.ojo@cwlgroup.com
 * date **: November 16, 2016  14:43 PM
 * -------------------------------------------------------------
 */
@Data
public class ApiResponse implements Serializable {

	//    private HttpStatus status;
	private int statusCode ;
	private boolean requestSuccessful;
	private ApiSuccess apiSuccess;// = new ApiSuccess();
	private double executionTime ;
	private ApiErrors apiErrors;
	private ApiWarnings apiWarnings;
	private String requestedCommand;

	public  <T> T getResponse(Class<T> clazz) {
		try {
			return clazz.cast(this);
		} catch(ClassCastException e) {
			return null;
		}
	}

}
