package com.dusty.commons.web.service.util;


import android.util.Base64;

import com.dusty.commons.util.AppUtil;

import java.io.IOException;
import java.security.KeyStore;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * @author .: Chukwudum Ekwueme
 * email ..: chidumekwueme@gmail.com, chukwudum.ekwueme@cwlgroup.com
 * created : May 13, 2016 11:35 AM
 */
public class ServiceGenerator {

    private static int READ_TIMEOUT = 60;
    private static int CONNECTION_TIMEOUT = 60;

    private static ServiceGenerator sInstance = null;



    private static OkHttpClient.Builder httpClient = null;
    private static  HttpLoggingInterceptor loggingInterceptor = null;



    private static Retrofit.Builder tokenBuilder =
            new Retrofit.Builder()
                  /*  .baseUrl(TOKEN_API_BASE_URL)*/
                    .addConverterFactory(GsonConverterFactory.create());

    private static Retrofit.Builder builder =
            new Retrofit.Builder()
                    /*.baseUrl(API_BASE_URL)*/
                    .addConverterFactory(GsonConverterFactory.create());

    public ServiceGenerator(){

    }

    private static SSLSocketFactory getSSLContext() throws Exception{

       // SSLContext sslContext = SSLContext.getInstance("TLS");
        SSLContext sslContext = SSLContext.getInstance("TLSv1.2");
        sslContext.init(null, null, null);
        /*if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
            noSSLv3Factory = new TLSSocketFactory(sslContext.getSocketFactory());
        } else {
            noSSLv3Factory = sslContext.getSocketFactory();
        }*/
        return new TLSSocketFactory(sslContext.getSocketFactory());
    }

    public static <S> S createService(Class<S> serviceClass, String baseUrl, boolean debugMode) {
       return createService(serviceClass, null ,baseUrl, debugMode);
    }

    public static <S> S createService(Class<S> serviceClass, String clientId,
                                      String clientSecret, String baseUrl,  boolean debugMode) {

        if(httpClient == null){
            httpClient = new OkHttpClient.Builder();
        }

        if (clientId != null && clientSecret != null) {
            String credentials = clientId + ":" + clientSecret;

            final String basic =
                    "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);

            httpClient.addInterceptor(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Request original = chain.request();

                    Request.Builder requestBuilder = original.newBuilder()
                            .header("Authorization", basic)
                            .header("Accept", "application/json")
                    .method(original.method(), original.body());

                    Request request = requestBuilder.build();
                    return chain.proceed(request);
                }
            });
        }

        tokenBuilder.baseUrl(baseUrl);

        if(debugMode){
            if(loggingInterceptor == null){
                loggingInterceptor = new HttpLoggingInterceptor();
            }
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            httpClient.interceptors().add(loggingInterceptor);
        }


        OkHttpClient client = httpClient.readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
                .connectTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS).build();

        Retrofit retrofit = tokenBuilder.client(client).build();
        return retrofit.create(serviceClass);
    }


    public static <S> S createService(Class<S> serviceClass, final String authToken, String baseUrl, boolean debugMode) {

        if(httpClient == null){
            httpClient = new OkHttpClient.Builder();
        }

        if (authToken != null) {
            httpClient.addInterceptor(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Request original = chain.request();

                    // Request customization: add request headers
                    Request.Builder requestBuilder = original.newBuilder()
                            .header("Authorization", "Bearer "+authToken)
                            .header("Accept", "application/json")
                            .method(original.method(), original.body());

                    Request request = requestBuilder.build();
                    return chain.proceed(request);
                }
            });
        }

        if(debugMode){
            if(loggingInterceptor == null){
                loggingInterceptor = new HttpLoggingInterceptor();
            }
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            httpClient.interceptors().add(loggingInterceptor);
        }

        //sets the base url based on the called from service
        builder.baseUrl(baseUrl);
       // if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
            try{
                AppUtil.debug("about to init custom trust manager");
                TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
                trustManagerFactory.init((KeyStore) null);
                TrustManager[] trustManagers = trustManagerFactory.getTrustManagers();
                if (trustManagers.length != 1 || !(trustManagers[0] instanceof X509TrustManager)) {
                    throw new IllegalStateException("Unexpected default trust managers:" + Arrays.toString(trustManagers));
                }
                X509TrustManager trustManager = (X509TrustManager) trustManagers[0];

                httpClient.sslSocketFactory(getSSLContext(), trustManager);

            }catch (Exception e){
                AppUtil.error("error adding the custom ssl context {} ", e.getMessage());
                e.printStackTrace();
            }
       // }


        OkHttpClient client = httpClient.readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
                .connectTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS).build();



        Retrofit retrofit = builder.client(client).build();
        return retrofit.create(serviceClass);
    }

}
