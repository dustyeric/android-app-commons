package com.dusty.commons.exception;

/**
 * @author .: Chukwudum Ekwueme
 * email ..: chidumekwueme@gmail.com, chukwudum.ekwueme@cwlgroup.com
 * created : Oct 01, 2018 9:20 AM
 */
public class ClientGenerationException extends Exception {
    public ClientGenerationException(String message){
        super(message);
    }
}
