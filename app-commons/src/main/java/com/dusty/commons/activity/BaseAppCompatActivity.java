package com.dusty.commons.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.ColorRes;
import android.support.annotation.DimenRes;
import android.support.annotation.IntegerRes;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.dusty.commons.helper.ToolBarHelper;
import com.dusty.commons.util.AppUtil;
import com.dusty.commons.util.DefaultSharedPrefHelper;
import com.dusty.commons.web.service.util.RestResultReciever;
import com.dusty.commons.web.service.util.ServiceConstants;

import java.util.HashMap;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

public class
BaseAppCompatActivity extends AppCompatActivity implements RestResultReciever.Receiver{

    protected Toolbar toolbar;
    protected ToolBarHelper toolBarHelper;

    @Getter
    @Setter
    protected AppUtil appUtil;
    @Getter
    protected RestResultReciever mReceiver;

    @Getter
    @Setter
    protected boolean enableLog;

    private HashMap<String, WebApiCallerInterface> callBacks = new HashMap<>();

    public int dimen(@DimenRes int resId) {
        return (int) getResources().getDimension(resId);
    }

    public int color(@ColorRes int resId) {
        return ContextCompat.getColor(this, resId);
    }

    public int integer(@IntegerRes int resId) {
        return getResources().getInteger(resId);
    }


    protected BaseAppCompatActivity getCommonsBaseActivity() {
        return this;
    }


    protected void log(String log, Object... parameters) {

        if (enableLog) {
            AppUtil.debug(AppUtil.Level.DEBUG, log, getClass().getSimpleName(), parameters);
        }

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mReceiver = new RestResultReciever(new Handler());
        mReceiver.setReceiver(this);

        SharedPreferences defaultSharedPreferences =
                PreferenceManager.getDefaultSharedPreferences(this);
        appUtil = new AppUtil(this, new DefaultSharedPrefHelper(this.getResources(),
                defaultSharedPreferences));

        //load the toolbar
    }

    protected void startActivityClosingOthers(Intent intent) {

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);

    }

    protected BaseAppCompatActivity setUpToolBar(String title, boolean enableBackPress) {
        ToolBarHelper.OnBackPressedListener onBackPressedListener = null;
        if (enableBackPress) {
            onBackPressedListener = new ToolBarHelper.OnBackPressedListener() {
                @Override
                public void onClick() {
                    finish();
                }
            };
        }

        setUpToolBar(title, onBackPressedListener);

        return this;
    }

    protected void setUpToolBar(String title) {
        setUpToolBar(title, null);
    }

    protected BaseAppCompatActivity setUpToolBar(String title, ToolBarHelper.OnBackPressedListener onBackPressedListener) {
        if (toolBarHelper != null) {

            toolBarHelper.title(title);
            if (onBackPressedListener != null) {
                toolBarHelper.enableBackPress(onBackPressedListener);
            }
        }

        return this;
    }

    public void setMyToolBarTitle(String title) {
        toolBarHelper.title(title);
    }

    protected BaseAppCompatActivity loadSharedWidgets(Activity activity, int toolbarResource, int backIconRes) {

        try {

            AppUtil.debug("about to find toolbar");
            toolbar = (Toolbar) activity.findViewById(toolbarResource);

            if (toolbar != null) {

                toolBarHelper = ToolBarHelper.build(this, toolbar, backIconRes);

            } else AppUtil.error("unable to find toolbar");

        } catch (Exception e) {

            AppUtil.error(e.getMessage());
        }

        return this;
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {

        AppUtil.log("received the code " + resultCode);
        String operation = resultData.getString(ServiceConstants.OPERATION);
        if (operation != null && !operation.equals("")) {
            //find a registered callback
            WebApiCallerInterface webApiCallerInterface = callBacks.get(operation);
            if (webApiCallerInterface != null) {
                webApiCallerInterface.onCallBackStart();
                if (resultCode == ServiceConstants.SUCCESS) {
                    webApiCallerInterface.onSuccess(resultData.get(ServiceConstants.SERVICE_RESPONSE_TAG));
                } else if (resultCode == ServiceConstants.FAILED) {
                    String error = resultData.getString(ServiceConstants.API_ERROR);
                    int errorCode = resultData.getInt(ServiceConstants.HTTP_ERROR_CODE);
                    webApiCallerInterface.onApiFailure(error, resultData.get(ServiceConstants.SERVICE_RESPONSE_TAG), errorCode);
                } else {
                    webApiCallerInterface.onNetworkFailure();
                }
            }
        } else {
            AppUtil.error("unknown operation , service call made without operation. override onReceived to get response");
        }
    }

    public void registerCallBack(String operation, WebApiCallerInterface webApiCallerInterface) {
        if (callBacks != null) {
            callBacks.put(operation, webApiCallerInterface);
        }
    }

    public interface MorphAnimationListener {
        void onAnimationEnd();
    }

    @Data
    public static abstract class WebApiCallerInterface<R, E> {

        public WebApiCallerInterface() {

        }

        public void onCallBackStart() {
            AppUtil.debug("callback started");
        }

        public void postFailure() {

        }

        public void postNetworkFailure() {

        }

        public void onApiUnavailable() {

        }

        public void onSuccess(R apiResponse) {

        }

        public void onApiFailure(String error, E errorResponse, int errorCode) {

            AppUtil.error(error);

            postFailure();

            if (errorResponse != null) {

                onReceiveErrorBody(errorCode, errorResponse);
            }
        }

        public void onReceiveErrorBody(int errorCode, E apiResponse) {
            AppUtil.error("received the error body " + apiResponse.toString());
        }

        public void onNetworkFailure() {

            AppUtil.error("network failure");
            // appUtil.toastLong("Server is unavailable");
            postNetworkFailure();
        }
    }
}