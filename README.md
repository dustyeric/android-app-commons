Android application utils 

to use add the following lines to your repository 

repositories {
    maven { url "https://dl.bintray.com/dustyeric/app-commons" }
}


implementation 'com.dusty.commons:app-commons:2.4.1'
implementation 'com.dusty.commons:commons-annotation:2.4.1'
annotationProcessor 'com.dusty.commons:commons-processor:2.4.1'